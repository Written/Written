## We've moved!

Written has moved from `Expo/Written` to `Written/Written` on Codeberg! If you've cloned the repo, please run the below command:

```sh
git remote set-url origin git@codeberg.org:Written/Written.git
```

---

<div align="center">

![Written Logo](https://codeberg.org/Written/Written/raw/branch/senpai/ico/written.png)

# Written

Filling the small gap between Appwrite and a perfect Developer Experience by providing an extensive suite of FOSS tools for Appwrite.

[![Codeberg](https://img.shields.io/badge/Codeberg-Written-724773.svg?style=flat-square)](https://codeberg.org/Written/Written)
[![License](https://img.shields.io/badge/License-MIT-724773.svg?style=flat-square)](https://codeberg.org/Written/Written/src/branch/senpai/LICENSE)

</div>

## Packages

### Templating Packages

The below packages are designed to be used in `pnpm create` or `npm init` commands, as they will initialize a Written project.

- [create-written](https://codeberg.org/Expo/Written/src/branch/senpai/packages/create-written) (`pnpm create written`, `npm init written`) - A simple, yet powerful, project initializer for Written projects. It will initialize a Written project with the base setup, and will install the necessary dependencies for you. You need an Appwrite instance to use this package. Uses `@written/base-monorepo-setup` under the hood.
- [@written/create-function](https://codeberg.org/Expo/Written/src/branch/senpai/packages/@written/create-function) (`pnpm create written-function`, `npm init written-function`) - A utility to create a new Written Function in your existing Written project.

### Core Packages

- [@written/app](https://codeberg.org/Expo/Written/src/branch/senpai/packages/@written/app) - A simple, yet powerful, Appwrite Function Framework. Allows you to have Routing and Middleware in your Appwrite Functions, similar to Koa or Express.
- [@written/appwrite-types](https://codeberg.org/Expo/Written/src/branch/senpai/packages/@written/appwrite-types) - TypeScript types for Appwrite's Functions, allowing you to gain typesafety over primitive Appwrite Functions. Designed for `@written/app`, but is perfectly usable in any Appwrite Function.

### Middleware Packages

The below packages are designed to be used with `@written/app` as middleware.

- [@written/cors-setup](https://codeberg.org/Expo/Written/src/branch/senpai/packages/@written/cors-setup) - A CORS wrapper, for use with `@written/wrap`. This is integrated into `@written/app` under `App.cors()`; the recommended way to use this.
- [@written/static](https://codeberg.org/Expo/Written/src/branch/senpai/packages/@written/static) - A microscopic static file server middleware for `@written/app`, allowing you to easily serve static files in your Appwrite Functions.
- [@written/throwables](https://codeberg.org/Expo/Written/src/branch/senpai/packages/@written/throwables) - Error handling made easy.

### Utility Packages

- [@written/mkctx](https://codeberg.org/Expo/Written/src/branch/senpai/packages/@written/mkctx) - A utility library for creating Appwrite Contexts, useful for testing, mocking and using Appwrite Functions outside of Appwrite.
- [@written/wrap](https://codeberg.org/Expo/Written/src/branch/senpai/packages/@written/wrap) - A tool for wrapping an Appwrite default function, as to modify either the request or response object. Works without written.
- [@written/httpcompat](https://codeberg.org/Expo/Written/src/branch/senpai/packages/@written/httpcompat) - Convert from/to node native http request/response objects to/from Appwrite's default function request/response objects.

#### Appwrite SDK Tooling

- [@written-client/sdk](https://codeberg.org/Expo/Written/src/branch/senpai/packages/@written-client/sdk) - Minimal wrapping around the Appwrite SDK with some utilities added.
- [@written-client/svelte](https://codeberg.org/Expo/Written/src/branch/senpai/packages/@written-client/svelte) - Svelte SDK, adding stores for authentication amongst other shenanigans.

#### Generic Function Utilities

- [@awfn/define-handler](https://codeberg.org/Expo/Written/src/branch/senpai/packages/@awfn/define-handler) - Simple alternative to `satisfies AppwriteDefault` that also gives you intellisense in plain-JS projects (and works fine in TS ones). Inspired by any configurations that have a `define<LibraryNameHere>` function.
- [@awfn/cli](https://codeberg.org/Expo/Written/src/branch/senpai/packages/@awfn/cli) - A helper for Appwrite Functions, allowing tasks like automatically redeploying your function on changes, amongst other things. Cleans up the `scripts` section of your functions' `package.json`s.

### Generic Packages

The below packages are not designed to be Appwrite-specific, and are designed for use anywhere.

- [@written/trailing-slash](https://codeberg.org/Expo/Written/src/branch/senpai/packages/@written/trailing-slash) - A simple utility to add or remove trailing slashes from URLs.
- [@written/route-parameters](https://codeberg.org/Expo/Written/src/branch/senpai/packages/@written/route-parameters) - Extracted Types from Express for converting route parameters to a type-safe params object.
- [@written/base-monorepo-setup](https://codeberg.org/Expo/Written/src/branch/senpai/packages/@written/base-monorepo-setup) - A simple, yet powerful, base monorepo setup library for `create-` packages, for making a simple `nx`-based Monorepo. Used in [`create-written`](https://codeberg.org/Expo/Written/src/branch/senpai/packages/create-written).

## Version

A large chunk of written is pre-1.x. This means that minor version bumps (`y` in v`x.y.z` increasing) may introduce breaking changes. We will try to keep this to a minimum, but it may happen.

Assuming your semver range is limited to the nearest patch version, written is considered stable for production use. Note that `create-written` **will limit to the nearest minor version**; please adjust this to the nearest patch if you plan to deploy to production prior to a ?=1.y.z of a package being released.

Exceptions to this are the following packages:

- [@written/throwables](https://codeberg.org/Expo/Written/src/branch/senpai/packages/@written/throwables)
- [@written/wrap](https://codeberg.org/Expo/Written/src/branch/senpai/packages/@written/wrap)
- [~~@written/appwrite-types~~](https://codeberg.org/Expo/Written/src/branch/senpai/packages/@written/appwrite-types) (will be stable in the very near future, hopefully)

These packages are considered stable for production use, and will not introduce breaking changes in minor version bumps.

Transitive dependencies are not considered stable for production use, even in these. If you depend on a dependency of a dependency, include it aswell; patch versions may bump major versions of transitive dependencies. (btw, please use a package manager like `pnpm` that can handle multiple versions of the same package)

## Notice

This is a 3rd-party project and, while being made for [Appwrite](https://appwrite.io/), is not affiliated with it.

## License

This project is licensed under the MIT License. See the [LICENSE](https://codeberg.org/Expo/Written/src/branch/senpai/LICENSE) file for details.
