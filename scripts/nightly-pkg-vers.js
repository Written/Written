import { execSync } from 'child_process';
import { readFileSync, writeFileSync } from 'fs';
if (!(process.env.USE_NIGHTLY_VERS || process.env.PUBLISH_THEN_REVERT))
  throw new Error(
    `This script makes everything use a -nightly version in the package.json's. Don't run it outside of CI.`,
  );

execSync('pnpm build', {
  stdio: 'inherit',
});

const HEAD = execSync('git describe --always').toString().trim();

const oldPkg = {};
const pkgs = execSync('nx exec -- pwd').toString().split('\n');
process.on('exit', () => {
  Object.entries(oldPkg).forEach(([pk, v]) => writeFileSync(pk, v));
});
pkgs.forEach(v => {
  oldPkg[v + '/package.json'] = readFileSync(v + '/package.json', 'utf-8');
  const pkg = JSON.parse(oldPkg);
  pkg.version = `${pkg.version}-${process.env.TAG ?? 'nightly'}-${HEAD}`;
  writeFileSync(v + '/package.json', JSON.stringify(pkg, null, 2));
});
execSync(
  'pnpm publish -r --filter "./packages/@written/*" --filter "./packages/*" --access public --tag ' +
    JSON.stringify(process.env.TAG ?? 'nightly'),
  {
    stdio: 'inherit',
  },
);
