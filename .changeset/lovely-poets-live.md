---
"@written/base-monorepo-setup": patch
"@written/route-parameters": patch
"@written/create-function": patch
"@written/appwrite-types": patch
"@written/trailing-slash": patch
"@written-client/svelte": patch
"@awfn/define-handler": patch
"@written-client/sdk": patch
"@written/cors-setup": patch
"@written/httpcompat": patch
"@written/throwables": patch
"@written/static": patch
"@written/mkctx": patch
"create-written": patch
"@written/wrap": patch
"@written/app": patch
"@awfn/cli": patch
---

Routine Dependency Upgrade
