---
"@written/base-monorepo-setup": patch
"@written/create-function": patch
"@written/appwrite-types": patch
"@written/trailing-slash": patch
"@awfn/define-handler": patch
"@written/cors-setup": patch
"@written/httpcompat": patch
"@written/throwables": patch
"@written/static": patch
"@written/mkctx": patch
"create-written": patch
"@written/wrap": patch
"@written/app": patch
"@awfn/cli": patch
---

- Move to a central "base" tsconfig.json
- Enable strict mode project-wide
- Skip Default Lib Check; Typescript's .d.ts files are assumed to be correct
