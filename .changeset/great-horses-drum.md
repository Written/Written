---
"@written/static": patch
---

The return type of appwriteStatic is a single promise type with an or'ed generic, not 2 promise types
This previously resulted in a strict-mode type-error :c
