#!/usr/bin/env node
import { minAppwriteVer, minNodeVer, updateChecker } from './create';
import { intro, select } from '@expove/clack__prompts';
import pkg from '../package.json';
import outro from './util/outro';
import { createWrittenProject } from './variants/project';
import { execSync } from 'child_process';
import semver from 'semver';
import consola from 'consola';
import { notCancel } from './util/notCancel';

process.on('beforeExit', () => {
  outro('Process is exiting unexpectedly!');
});

(async () => {
  intro(`${pkg.name} v${pkg.version}`);
  await updateChecker();
  if (semver.compare(process.versions.node, minNodeVer) === -1) {
    outro(
      `Too old node version! Expected >= ${minNodeVer}, got ${process.versions.node}`,
    );
    process.exit(3);
  }
  if (!process.env.FORCE_SKIP_APPWRITE_VERSION_CHECK_DANGER_DRAGONS_AHEAD)
    try {
      if (
        semver.compare(
          execSync(
            'appwrite --version' +
              (process.platform === 'linux' || process.platform === 'freebsd'
                ? ' 2>/dev/null'
                : ''),
          ).toString('utf-8'),
          minAppwriteVer,
        ) === -1
      ) {
        outro('Appwrite CLI version is unsupported.', true);
        consola.error(
          `Appwrite CLI is below version ${minAppwriteVer}. Refusing to run. Upgrade your CLI.`,
        );
        process.exit(4);
      }
    } catch (error) {
      outro('Could not run appwrite version command', true);
      consola.error(
        'Failed to perform appwrite cli version check! Is it installed?',
        error,
      );
      consola.info(
        `More info on Appwrite's CLI: https://appwrite.io/docs/tooling/command-line/installation`,
      );
      process.exit(5);
    }
  const type = process.env.PROJECT_TYPE ?? 'project';
  // ?? notCancel(
  //   await select({
  //     message: 'Which kind of project do you want to create?',
  //     options: [
  //       {
  //         label: 'Root Project',
  //         value: 'project',
  //         hint: `You're most likely looking for this.`,
  //       },
  //       {
  //         label: 'Appwrite Function',
  //         value: 'appwrite-func',
  //         hint: `Select this option if you're running this from inside an existing @written workspace.`,
  //       },
  //     ],
  //   }),
  // );
  switch (type) {
    case 'project':
      await createWrittenProject();
      return;

    default:
      return outro('Not a valid project type: ' + JSON.stringify(type), true);
  }
})();
