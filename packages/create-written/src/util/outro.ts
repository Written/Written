import { outro as out, cancel } from '@expove/clack__prompts';
let outroCalled = false;
export const outro = (msg?: string, isCancel = false) => {
  if (outroCalled) return;
  outroCalled = true;
  return isCancel ? cancel(msg) : out(msg);
};
export default outro;
