import semver from 'semver';
import pkg from '../../package.json';
import { confirm } from '@expove/clack__prompts';
import { consola } from 'consola';
import spinner from './spinner';
import outro from './outro';

/**
 * Performs the update check, logging to console any responses. Must be awaited, result is always void.
 * If outdated, the user is prompted to not continue, at which point the process is exited with status code 2.
 */
export const updateChecker = async () => {
  spinner.start('Initializing Update Check');
  if (!process.env.SKIP_VERSION_CHECK) {
    spinner.message(
      'Performing Version Check - Set env.SKIP_VERSION_CHECK to skip.',
    );
    const pkgName = pkg.name;
    const registryResponse = await fetch(
      `https://registry.npmjs.org/${pkgName}/latest`,
    );
    if (registryResponse.ok) {
      const json = (await registryResponse.json()) as typeof pkg;
      const compareResult = semver.compare(json.version, pkg.version);
      if (compareResult === 1) {
        spinner.stop(
          `Outdated Version! Your version is ${pkg.version}, latest stable is ${json.version}.`,
        );
        await new Promise<null>(rs => setTimeout(() => rs(null), 10));
        if (
          !(await confirm({
            message:
              'Are you sure you want to continue on an out-of-date version?',
            initialValue: false,
          }))
        ) {
          outro('Bye!', true);
          consola.error(
            `Did not continue on an outdated version at the user's request.`,
          );
          process.exit(2);
        }
      } else if (compareResult === -1)
        spinner.message(
          `You're ahead of stable! Latest Stable is ${json.version}, current is ${pkg.version}.`,
        );
      else spinner.stop(`You're on the latest stable version!`);
    } else
      spinner.stop(
        `Failed Update Check with status code ${registryResponse.status}: ${JSON.stringify(registryResponse.statusText)}`,
      );
  } else spinner.stop('Found SKIP_VERSION_CHECK - Skipping Version Check!');
};
export default updateChecker;
