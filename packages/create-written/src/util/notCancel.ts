import { isCancel } from '@expove/clack__prompts';
import outro from './outro';
export const notCancel = <T extends any>(val: T | symbol) => {
  if (isCancel(val)) {
    outro('Cancelling', true);
    process.exit(0);
  } else return val as T;
};
