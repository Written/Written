import updateChecker from './util/update';
import pkg from '../package.json';
import baseProject from './files/base-project';
import nx from './files/nx';
import rootDependencies from './files/root-dependencies';
import rootPackage from './files/root-pkg';
import generateTsConf from './files/tsconf';

export const minAppwriteVer = '4.2.0';
export const minNodeVer = '20.10.0';

export { updateChecker, pkg as packageJson };
export const modules = {
  baseProject,
  baseNx: nx,
  rootDependencies,
  rootPackage,
  tsConfig: generateTsConf,
};
