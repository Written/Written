export default {
  targets: {
    build: {
      dependsOn: [
        {
          dependencies: true,
          target: 'build',
          params: 'ignore',
        },
      ],
    },
  },
};
