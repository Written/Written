import optFunc from '../util/optFunc';
import slug from './slug';

export const rootPkg = optFunc(
  opt => ({
    name: slug(opt.name),
    displayName: opt.name,
    version: '0.1.0',
    description: opt.description,
    scripts: {
      'function-deploy': 'nx run-many -t aw-deploy --parallel=1',
      'new-func': 'pnpm create @written/function',
      appwrite: 'cd packages/appwrite && appwrite',
      build: 'nx run-many -t build --parallel=16',
      test: 'nx run-many -t test --parallel=64',
      lint: 'nx run-many -t lint --parallel=8192',
      dev: 'nx run-many -t dev --parallel=8192',
    },
    keywords: [],
    author: 'Author',
    contributors: [
      {
        name: 'Author',
        url: 'https://codeberg.org/author-codeberg-username',
        email: 'maintainer@contact.email',
      },
    ],
    maintainers: [
      {
        name: 'Author',
        url: 'https://codeberg.org/author-codeberg-username',
        email: 'maintainer@contact.email',
      },
    ],
    repository: {
      type: 'git',
      url: 'https://codeberg.org/author-codeberg-username/repository-name.git',
    },
    homepage: 'https://codeberg.org/author-codeberg-username/repository-name',
    bugs: {
      url: 'https://codeberg.org/author-codeberg-username/repository-name/issues',
      email: 'maintainer@contact.email',
    },
    license: 'MIT',
    engines: {
      node: '>=v20.0.0',
    },
    sideEffects: false,
    type: 'module',
    private: true,
  }),
  {
    name: 'written-project',
    description: 'A @written project.',
  },
);
export default rootPkg;
