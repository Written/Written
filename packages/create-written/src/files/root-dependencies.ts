export const rootDependencies = [
  '@3xpo/es-many',
  '@nx/js',
  'concurrently',
  'esbuild',
  'nodemon',
  'nx',
  'prettier',
  'prettier-plugin-svelte',
  'prompts',
  'ts-node',
  'svelte',
  'typescript',
  '@types/node',
];
export default rootDependencies;
