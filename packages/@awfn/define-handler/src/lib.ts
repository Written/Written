import AppwriteDefault from '@written/appwrite-types';
export const defineHandler = <T extends AppwriteDefault>(handler: T) => handler;
export default defineHandler;
