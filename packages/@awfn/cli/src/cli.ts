#!/usr/bin/env node
import ArgParser from '@3xpo/argparser';
import { execSync } from 'child_process';
import consola from 'consola';
import fs from 'fs';
import path from 'path';
import chokidar from 'chokidar';

const argv = ArgParser.hideBin(process.argv);
const command = argv.shift()!;

const watchDirectory = (
  directoryPath: string,
  callbackify: (
    eventName: 'add' | 'addDir' | 'change' | 'unlink' | 'unlinkDir',
    path: string,
    stats?: fs.Stats | undefined,
  ) => void,
) =>
  chokidar
    .watch(
      [
        path.join(
          directoryPath,
          process.env.SKIP_BUILD_BEFORE_DEPLOY ? 'dist' : 'src',
        ),
        path.join(directoryPath, 'package.json'),
      ],
      {
        awaitWriteFinish: true,
        depth: 16,
      },
    )
    .on('all', callbackify);

(async () => {
  if (!path.resolve(process.cwd(), '..').endsWith('functions'))
    return consola.error(
      new Error('Cannot run aw-fn from outside of a function!'),
    );
  if (!fs.existsSync(path.resolve(process.cwd(), 'package.json')))
    return consola.error(
      new Error(
        `Must run aw-fn in a function's directory! Missing package.json.`,
      ),
    );
  const pkg = JSON.parse(
    fs.readFileSync(path.join(process.cwd(), 'package.json'), 'utf-8'),
  );

  const buildCmd = () =>
    `esbuild --bundle --format=cjs --platform=node --outfile=${JSON.stringify(pkg.main ?? 'dist/main.js')} --sourcemap=inline src/main.ts ${argv.map(v => JSON.stringify(v)).join(' ')}`;
  const build = () =>
    execSync(buildCmd(), {
      stdio: 'inherit',
      cwd: process.cwd(),
    });
  const deployCmd = () =>
    `pnpm appwrite deploy function --functionId ${JSON.stringify(pkg.name)}`;
  const deploy = () =>
    execSync(deployCmd(), {
      stdio: 'inherit',
      cwd: path.resolve(process.cwd(), '..', '..'),
    });

  switch (command) {
    case 'build':
      build();
      break;

    case 'deploy':
      build();
      deploy();
      break;

    case 'watch':
      if (process.env.DEPLOY_IMMEDIATELY) {
        if (!process.env.SKIP_BUILD_BEFORE_DEPLOY) build();
        deploy();
      }
      let watchStart = performance.now();
      watchDirectory(process.cwd(), (action, path) => {
        const delta = performance.now() - watchStart;
        if (delta < 200) return;
        watchStart += delta;

        if (path.includes('dist/') || path.includes('dist\\')) return;
        if (!process.env.SKIP_BUILD_BEFORE_DEPLOY) build();
        deploy();
      });
      break;

    case 'remove':
      consola.error(
        'TODO: Add remove command for removing the current function cleanly.',
      );
      break;

    default:
      throw new Error(`unsupported command: ${JSON.stringify(command)}`);
  }
})();
