<div align="center">

![Written Logo](https://codeberg.org/Written/Written/raw/branch/senpai/ico/written.png)

# @awfn/cli

Appwrite/Written CLI Utilities & Command Wrappers. This is mainly designed for use with [`@written/create-function`](https://codeberg.org/Written/Written/src/branch/senpai/packages/@written/create-function).

[![Codeberg](https://img.shields.io/badge/Codeberg-Written-724773.svg?style=flat-square)](https://codeberg.org/Written/Written/src/branch/senpai/packages/@awfn/define-handler)
[![License](https://img.shields.io/badge/License-MIT-724773.svg?style=flat-square)](https://codeberg.org/Written/Written/src/branch/senpai/LICENSE)

</div>

## Installation

In your function, run:

```sh
pnpm i @awfn/cli -D
```

## Usage

```sh
# Build the current Written function
aw-fn build;

# Build & Deploy the current Written function
aw-fn deploy;

# Watch the current Written function for changes, rebuilding & deploying on change.
aw-fn watch; # set DEPLOY_IMMEDIATELY to also deploy immediately when running, and set SKIP_BUILD_BEFORE_DEPLOY to skip building before deploying.
```
