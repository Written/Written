# @awfn/cli

## 0.1.14

### Patch Changes

- a748109: Update Deps

## 0.1.13

### Patch Changes

- 51d9506: Upgrade Dependencies

## 0.1.12

### Patch Changes

- d1ea241: Update Deps

## 0.1.11

### Patch Changes

- 77b7db6: Watch src when not skipping build

## 0.1.10

### Patch Changes

- 4fa1758: Upgrade dependencise
- a0c9930: Upgrade Dependencies

## 0.1.9

### Patch Changes

- ignore dist in watch

## 0.1.8

### Patch Changes

- 3872f26: Add LICENSE files
- bee1c23: Move repos to Written/Written
- 80be314: Add an env var for certain functionality to watch
- 80be314: New doc format

## 0.1.7

### Patch Changes

- 40f46fa: Upgrade Dependencies

## 0.1.6

### Patch Changes

- 4a5dd4d: bump dependencies

## 0.1.5

### Patch Changes

- 7869ef7: use a shebang

## 0.1.4

### Patch Changes

- awfn/cli is too small to bother minifying

## 0.1.3

### Patch Changes

- rebuild everything due to a filesystem error during the last build

## 0.1.2

### Patch Changes

- 2053468: watch the dir ourselves

## 0.1.1

### Patch Changes

- bump everything
