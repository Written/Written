# @written-client/svelte

## 0.1.6

### Patch Changes

- a748109: Update Deps
  - @written-client/sdk@0.2.4

## 0.1.5

### Patch Changes

- 39f46a0: modern appwrite uses createEmailPasswordSession (why???)

## 0.1.4

### Patch Changes

- 86be942: upgrade vite

## 0.1.3

### Patch Changes

- 51d9506: Upgrade Dependencies
- Updated dependencies [51d9506]
  - @written-client/sdk@0.2.3

## 0.1.2

### Patch Changes

- d1ea241: Update Deps
- Updated dependencies [d1ea241]
  - @written-client/sdk@0.2.2

## 0.1.1

### Patch Changes

- @written-client/sdk@0.2.1

## 0.1.0

### Minor Changes

- ba2623e: Upgrade to Appwrite@14, upgrade alll other packages

### Patch Changes

- a0c9930: Upgrade Dependencies
- Updated dependencies [8ced11d]
- Updated dependencies [ba2623e]
  - @written-client/sdk@0.2.0

## 0.0.5

### Patch Changes

- @written-client/sdk@0.1.4

## 0.0.4

### Patch Changes

- 3872f26: Add LICENSE files
- bee1c23: Move repos to Written/Written
- de25848: Consistent, Clean README header format
- Updated dependencies [3872f26]
- Updated dependencies [bee1c23]
- Updated dependencies [de25848]
  - @written-client/sdk@0.1.3

## 0.0.3

### Patch Changes

- 40f46fa: Upgrade Dependencies
- 40f46fa: Use firefox in testing
- Updated dependencies [40f46fa]
  - @written-client/sdk@0.1.2

## 0.0.2

### Patch Changes

- 4a5dd4d: bump dependencies
- Updated dependencies [4a5dd4d]
  - @written-client/sdk@0.1.1
