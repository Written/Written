import { SDK as BaseSDK } from '@written-client/sdk';
import { type AnyFunc } from '@3xpo/events';
import Account from './account.ts';
import type { Models } from 'appwrite';

export type SDKEvents = {
  loggedIn: (user: Models.User<Models.Preferences>) => void;
  loggedOut: () => void;
};

/**
 * Written Svelte SDK
 * @example ```ts
 * import SDK from '@written-client/svelte';
 * const sdk = new SDK()
 *                          .endpoint('https://example.co/v1')
 *                          .project('example-proj-id');
 * ```
 */
export default class SDK<
  AdditionalEvents extends {
    [Key in string]: AnyFunc;
  } = {},
> extends BaseSDK<
  {
    [K in keyof AdditionalEvents]: AdditionalEvents[K];
  } & {
    [K in keyof SDKEvents]: SDKEvents[K];
  }
> {
  protected _account?: Account;
  /**
   * Creates a wrapped Account instance, utilizing Svelte Stores to handle logins
   */
  public get account(): Account {
    return (this._account = this._account ?? new Account(this as SDK));
  }
}
const sdk = new SDK();
