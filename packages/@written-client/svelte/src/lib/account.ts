import type SDK from './sdk.ts';
import {
  Account as AppwriteAccount,
  AppwriteException,
  ID,
  type Models,
} from 'appwrite';
import { writable, type Writable } from 'svelte/store';
export default class Account extends AppwriteAccount {
  public constructor(public sdk: SDK) {
    super(sdk.client);
    // Ensure the value is ALWAYS up to date.
    this.store.subscribe(value => {
      this.value = value;
    });
  }
  /** The internal getUser; always forcing, will run even if another promise is active */
  protected async rawGetUser() {
    try {
      const acc = await this.get();
      this.store.set(acc);
      this.value = acc;
      this.sdk.emit('loggedIn', acc);
      return acc;
    } catch (error) {
      if (error instanceof AppwriteException) {
        if (
          (error.stack ?? error.message ?? `${error}`).includes(
            'missing scope (account)',
          )
        ) {
          this.store.set(null);
          this.value = null;
          this.sdk.emit('loggedOut');
          return null;
        } else throw error;
      } else
        throw new Error('Encountered unexpected error during login check', {
          cause: error,
        });
    }
  }
  protected getUserPromise: ReturnType<typeof this.rawGetUser> | undefined =
    undefined;
  /**
   * Call this function after the page loads to check if the user has logged in or not.
   * @param {boolean} [force=false] If we should re-fetch even if there's already a value
   * @param {boolean} [wait=true] If we should wait for all existing fetch before returning - only matters if force=true - if false, race conditions can happen
   */
  public async getUser(force: boolean = false, wait: boolean = true) {
    if ((wait || !force) && this.getUserPromise) {
      if (!force) return await this.getUserPromise;
      else while (this.getUserPromise) await this.getUserPromise;
    }
    if (!force && this.value !== undefined) return this.value;
    this.getUserPromise = this.rawGetUser();
    const rs = await this.getUserPromise;
    this.getUserPromise = undefined;
    return rs;
  }
  /** Alias to getUser */
  public init = this.getUser;
  /**
   * The last value of the store; the current account - see below:
   *
   * null = no account
   *
   * undefined = pending account fetch (call {@link getUser} if on client!)
   *
   * any other value = the account
   */
  public value: Models.User<Models.Preferences> | null | undefined = undefined;
  /** Internal value for {@link this.__store} */
  protected store = writable<typeof this.value>();
  /** The internal store, I'd avoid touching it unless you're doing custom shit - null = no account, undefined = pending, otherwise value is account */
  public get __store() {
    return this.store;
  }
  /**
   * null = no account
   *
   * undefined = pending account fetch (call {@link getUser} if on client!)
   *
   * any other value = the account
   */
  public subscribe = this.store.subscribe;
  /** Saves an auth cookie string, handling cookieFallback - Useful for custom login/register functions */
  public saveCookie(responseCookie: string) {
    const setCookie = responseCookie.replace(/;? ?httponly/giu, '');
    const cookies = setCookie.split(/, ?(?=a_?[a-z_]+=)/);
    document.cookie = setCookie;
    cookies.forEach(thisCookie => (document.cookie = thisCookie));
    localStorage.setItem(
      'cookieFallback',
      JSON.stringify({
        ...JSON.parse(localStorage.getItem('cookieFallback') ?? '{}'),
        ...Object.fromEntries(
          cookies
            .map(v => v.split(/;/giu)[0])
            .map(v => v.split('='))
            .map(v => [v.shift(), v.join('=')] as const),
        ),
      }),
    );
  }
  /**
   * Logs into an account.
   *
   * You can change this to a custom value if necessary. Just make sure to call {@link getUser this.getUser(force: true)} or {@link Writable.set this.__store.set(account)}
   *
   * You *must* debounce the default value of this, otherwise you'll encounter store race conditions.
   */
  public async login(email: string, password: string) {
    await this.createEmailPasswordSession(email, password);
    await this.init(true);
  }
  /**
   * Registers an account.
   *
   * You should change this to a custom value - preferably one that calls an Appwrite Function to handle DB setup & such. Just make sure to call {@link getUser this.getUser(force: true)} or {@link Writable.set this.__store.set(account)}
   *
   * You *must* debounce the default value of this, otherwise you'll encounter store race conditions.
   *
   * You *should* call {@link Account.login} after calling this.
   */
  public register = async (
    email: string,
    password: string,
    name?: string,
    id = ID.unique(),
  ) => {
    await this.create(id, email, password, name);
  };
}
