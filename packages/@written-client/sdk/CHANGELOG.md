# @written-client/sdk

## 0.2.4

### Patch Changes

- Updated dependencies [a748109]
  - @written/appwrite-types@0.2.17

## 0.2.3

### Patch Changes

- 51d9506: Upgrade Dependencies
- Updated dependencies [51d9506]
  - @written/appwrite-types@0.2.16

## 0.2.2

### Patch Changes

- d1ea241: Update Deps
- Updated dependencies [d1ea241]
  - @written/appwrite-types@0.2.15

## 0.2.1

### Patch Changes

- Updated dependencies [1df6d84]
  - @written/appwrite-types@0.2.14

## 0.2.0

### Minor Changes

- ba2623e: Upgrade to Appwrite@14, upgrade alll other packages

### Patch Changes

- 8ced11d: Check execution method and warn on mismatch
- Updated dependencies [034c27f]
- Updated dependencies [4fa1758]
- Updated dependencies [39ce77a]
- Updated dependencies [a0c9930]
  - @written/appwrite-types@0.2.13

## 0.1.4

### Patch Changes

- Updated dependencies [816cec3]
  - @written/appwrite-types@0.2.12

## 0.1.3

### Patch Changes

- 3872f26: Add LICENSE files
- bee1c23: Move repos to Written/Written
- de25848: Consistent, Clean README header format
- Updated dependencies [8acd2e0]
- Updated dependencies [bee1c23]
- Updated dependencies [de25848]
  - @written/appwrite-types@0.2.11

## 0.1.2

### Patch Changes

- 40f46fa: Upgrade Dependencies
- Updated dependencies [40f46fa]
  - @written/appwrite-types@0.2.10

## 0.1.1

### Patch Changes

- 4a5dd4d: bump dependencies
- Updated dependencies [4a5dd4d]
  - @written/appwrite-types@0.2.9
