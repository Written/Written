<div align="center">

![Written Logo](https://codeberg.org/Written/Written/raw/branch/senpai/ico/written.png)

# @written-client/sdk

General SDK for @written client things

[![Codeberg](https://img.shields.io/badge/Codeberg-Written-724773.svg?style=flat-square)](https://codeberg.org/Written/Written/src/branch/senpai/packages/@written-client/sdk)
[![License](https://img.shields.io/badge/License-MIT-724773.svg?style=flat-square)](https://codeberg.org/Written/Written/src/branch/senpai/LICENSE)

</div>

## Installation

In your client package, run:

```sh
pnpm i @written-client/sdk
```

## Usage

TODO: Add usage instructions. For now, just use it as if it was the standard `appwrite` package - intellisense should help you out for the custom stuff.
