# @written/appwrite-types

## 0.2.17

### Patch Changes

- a748109: Update Deps

## 0.2.16

### Patch Changes

- 51d9506: Upgrade Dependencies

## 0.2.15

### Patch Changes

- d1ea241: Update Deps

## 0.2.14

### Patch Changes

- 1df6d84: Add some JSDocs

## 0.2.13

### Patch Changes

- 034c27f: Make the AppwriteRequestHeaders type include a Record<string,string>, splitting just the platform object into a separate object
- 4fa1758: Upgrade dependencise
- 39ce77a: Add appwrite headers to type
- a0c9930: Upgrade Dependencies

## 0.2.12

### Patch Changes

- 816cec3: remove route-parser dependency - i have no clue why it was there

## 0.2.11

### Patch Changes

- 8acd2e0: Update License Year
- bee1c23: Move repos to Written/Written
- de25848: Consistent, Clean README header format

## 0.2.10

### Patch Changes

- 40f46fa: Upgrade Dependencies

## 0.2.9

### Patch Changes

- 4a5dd4d: bump dependencies

## 0.2.8

### Patch Changes

- rebuild everything due to a filesystem error during the last build

## 0.2.7

### Patch Changes

- bump everything

## 0.2.6

### Patch Changes

- 18c6dd1: fix: use es6 in lib, i hate you typescript

## 0.2.5

### Patch Changes

- 1208d0d: Strict Mode, for everything

## 0.2.4

### Patch Changes

- bca8b56: Use Strict Null Checks

## 0.2.3

### Patch Changes

- c09e234: Add LICENSE to npm files

## 0.2.2

### Patch Changes

- 9af1ae7: Mark as side-effect-free
- b7955f5: Fix a quite critical typo

## 0.2.1

### Patch Changes

- Bump Version

## 0.2.0

### Minor Changes

- ad89eae: Migrate to nx

### Patch Changes

- ad89eae: Fix a stupid circular dependency

## 0.1.6

### Patch Changes

- Updated dependencies [e1c8c20]
  - @written/cors-setup@0.2.2
  - @written/wrap@0.1.6
  - @written/appwrite-types@0.1.6

## 0.1.5

### Patch Changes

- Updated dependencies [8917383]
  - @written/cors-setup@0.2.1
  - @written/wrap@0.1.5
  - @written/appwrite-types@0.1.5

## 0.1.4

### Patch Changes

- 96f55e2: Add Documentation to Appwrite-Types
- Updated dependencies [202b68e]
- Updated dependencies [d296d3c]
- Updated dependencies [3cc92a2]
- Updated dependencies [96f55e2]
  - @written/wrap@0.1.4
  - @written/cors-setup@0.2.0
  - @written/appwrite-types@0.1.4

## 0.1.3

### Patch Changes

- d23e286: Remove unnecessary axios dependencies (idk why it was there)
- Updated dependencies [d23e286]
  - @written/appwrite-types@0.1.3
  - @written/cors-setup@0.1.3
  - @written/wrap@0.1.3

## 0.1.2

### Patch Changes

- cbe6bed: Make homepage be the specific project repo dir
- Updated dependencies [cbe6bed]
  - @written/appwrite-types@0.1.2
  - @written/cors-setup@0.1.2
  - @written/wrap@0.1.2

## 0.1.1

### Patch Changes

- 50e20ad: Initial Changeset
- Updated dependencies [50e20ad]
  - @written/appwrite-types@0.1.1
  - @written/cors-setup@0.1.1
  - @written/wrap@0.1.1
