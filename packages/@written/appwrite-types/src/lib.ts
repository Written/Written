// Reference: https://github.com/open-runtimes/open-runtimes/blob/main/runtimes/node-20.0

import { z } from 'zod';

/** HTTP 3xx Redirect Codes - Utility Object */
export const RedirectCodes = {
  /** Indicates multiple options for the resource from which the client may choose (via {@link https://en.wikipedia.org/wiki/Content_negotiation#Agent-driven agent-driven content negotiation}). */
  MultipleChoices: 300,
  /** Moved Permanently - This and all future requests should be directed to the provided location. {@link https://en.wikipedia.org/wiki/HTTP_301 More Info} */
  MovedPermanently: 301,
  /** Found (previously Moved Temporarily) - Tells the client to look at (browse to) another URL. The HTTP/1.0 specification required the client to perform a temporary redirect with the same method (the original describing phrase was "Moved Temporarily"), but popular browsers implemented 302 redirects by changing the method to GET. Therefore, HTTP/1.1 added status codes 303 and 307 to distinguish between the two behaviours. */
  Found: 302,
  /** See Other (HTTP/1.1) - The response to the request can be found under another URI using the GET method. When received in response to a POST (or PUT/DELETE), the client should presume that the server has received the data and should issue a new GET request to the given URI. */
  SeeOther: 303,
  /** Not Modified - Indicates that the resource has not been modified since the version specified by the request headers If-Modified-Since or If-None-Match. In such case, there is no need to retransmit the resource since the client still has a previously-downloaded copy. */
  NotModified: 304,
  /** Use Proxy (HTTP/1.1) - The requested resource is available only through a proxy, the address for which is provided in the response. For security reasons, many HTTP clients (such as Firefox amongst other browsers) do not obey this status code. */
  UseProxy: 305,
  /** Switch Proxy - No longer used. Originally meant "Subsequent requests should use the specified proxy." */
  SwitchProxy: 306 as never,
  /** Temporary Redirect (HTTP/1.1) - In this case, the request should be repeated with another URI; however, future requests should still use the original URI. In contrast to how 302 was historically implemented, the request method is not allowed to be changed when reissuing the original request. For example, a POST request should be repeated using another POST request. */
  TemporaryRedirect: 307,
  /** Permanent Redirect - This and all future requests should be directed to the given URI. 308 parallel the behaviour of 301, but does not allow the HTTP method to change. So, for example, submitting a form to a permanently redirected resource may continue smoothly. */
  PermanentRedirect: 308,
} as const;

export const Scheme = z.enum(['http', 'https']);
export type Scheme = z.infer<typeof Scheme>;
export const AppwriteTrigger = z.enum([
  /** The function was triggered by an HTTP Request or createExecution */
  'http',
  /** The function was triggered by an Appwrite Event */
  'event',
  /** The function was triggered due to a cron schedule */
  'schedule',
  /** Internal default type; appwrite doesnt appear to ever give this - this is set as the default in the schema so there's some value when using things like unit testing so zod doesnt die */
  '$unknown',
]);
export type AppwriteTrigger = z.infer<typeof AppwriteTrigger>;
/** Appwrite Request Headers; headers appwrite adds to requests */
export const AppwriteRequestPlatformHeaders = z.object({
  /** Describes how the function execution was invoked. Possible values are http, schedule or event. */
  'x-appwrite-trigger': AppwriteTrigger.optional().default('$unknown'),
  /** If the function execution was triggered by an event, describes the triggering event. */
  'x-appwrite-event': z.string().optional(),
  /** If the function execution was invoked by an authenticated user, display the user ID. This doesn't apply to Appwrite Console users or API keys. */
  'x-appwrite-user-id': z.string().optional(),
  /** JWT token generated from the invoking user's session. Used to authenticate Server SDKs to respect access permissions. {@link https://appwrite.io/docs/products/auth/jwt Learn more about JWT tokens}. */
  'x-appwrite-user-jwt': z.string().optional(),
  /** Displays the country code of the configured locale. */
  'x-appwrite-country-code': z.string().optional(),
  /** Displays the continent code of the configured locale. */
  'x-appwrite-continent-code': z.string().optional(),
  /** If the user is in the EU or not - Zod defaults this to false when not specified */
  'x-appwrite-continent-eu': z
    .literal('true')
    .or(z.literal('false'))
    .optional()
    .default('false'),
});
export type AppwriteRequestPlatformHeaders = z.infer<
  typeof AppwriteRequestPlatformHeaders
>;
export const AppwriteRequestHeaders = z
  .record(z.string(), z.string())
  .and(AppwriteRequestPlatformHeaders);
export type AppwriteRequestHeaders = z.infer<typeof AppwriteRequestHeaders>;
export const AppwriteReq = z.object({
  bodyRaw: z.string(),
  /** JSON if contentType is application/json */
  body: z.any(),
  /** Lowercase Key-Value Index of headers */
  headers: AppwriteRequestHeaders,
  /** Request method, such as GET, POST, PUT, DELETE, PATCH, etc... */
  method: z.string(),
  /** Hostname from the host header, such as awesome.appwrite.io - does not include port */
  host: z.string(),
  /** Value of the x-forwarded-proto header, usually `http` or `https` */
  scheme: Scheme,
  /** Parsed query params. For example, req.query.limit */
  query: z.record(z.string(), z.string()),
  /** Raw query params string. For example "limit=12&offset=50" */
  queryString: z.string(),
  /** The port to use - coerced to a `string` in builtin appwrite. When using zod, coerced back to a number. */
  port: z.coerce.number().min(0).max(65536),
  /** Path part of URL, for example /ur/route/v1/abc/hooks */
  url: z.string(),
  /** The path */
  path: z.string(),
});
export type AppwriteReq = z.infer<typeof AppwriteReq> & {
  /** The port - appears to be a string|number Something feels off, someone needs to type openruntime */
  port?: string | number;
};

export const AppwriteResponseObject = z.object({
  body: z.any(),
  statusCode: z
    .number()
    .min(100, 'Status Code must be atleast 1xx')
    .max(599, 'Status code must be at most 599'),
  headers: z.record(z.string(), z.string()),
});
export type AppwriteResponseObject = z.infer<typeof AppwriteResponseObject>;

/** ctx.res */
export type AppwriteRes = {
  /** Get a raw data response object */
  send: (
    body: any,
    statusCode?: number,
    headers?: Record<string, string>,
  ) => AppwriteResponseObject;
  /** Get a JSON data response object */
  json: (
    obj: Record<any, any>,
    statusCode?: number,
    headers?: Record<string, string>,
  ) => AppwriteResponseObject;
  /** Get an empty response object */
  empty: () => AppwriteResponseObject & {
    body: '';
    statusCode: 204;
    headers: {};
  };
  /** Get a redirection object */
  redirect: <
    Target extends string,
    Code extends number = 301,
    Headers extends Record<string, string> = {},
  >(
    url: Target,
    statusCode?: Code,
    headers?: Headers,
  ) => AppwriteResponseObject & {
    body: '';
    headers: Headers & {
      location: Target;
    };
    statusCode: Code;
  };
};

export type AppwriteContext = {
  /** Request Info */
  req: AppwriteReq;
  /** Response helpers */
  res: AppwriteRes;
  /** Log something! */
  log: (message: any) => void;
  /** Send an error message! */
  error: (message: any) => void;
};

export type AppwriteDefault = (
  ctx: AppwriteContext,
) => Promise<AppwriteResponseObject> | AppwriteResponseObject;

export default AppwriteDefault;
