<div align="center">

![Written Logo](https://codeberg.org/Written/Written/raw/branch/senpai/ico/written.png)

# @written/appwrite-types

Typesafety brought to Appwrite Functions.

[![Codeberg](https://img.shields.io/badge/Codeberg-Written-724773.svg?style=flat-square)](https://codeberg.org/Written/Written/src/branch/senpai/packages/@written/appwrite-types)
[![License](https://img.shields.io/badge/License-MIT-724773.svg?style=flat-square)](https://codeberg.org/Written/Written/src/branch/senpai/LICENSE)

</div>

## Installation

Run `pnpm i -D @written/appwrite-types` in your function.

## Usage

```ts
import type AppwriteFunction from '@written/appwrite-types';

export default (ctx => {
  // ...
  return ctx.json(
    {
      success: true,
      message: 'Hello, World!',
    },
    200,
    {
      'x-test': '1',
    },
  );
}) satisfies AppwriteFunction;
```

## Note

A lot of [`@written`](https://codeberg.org/Expo/Written) packages use this for typesafety. If you're using one of these, you will want this package installed, however you will not need to ever directly interact with it.

This is a peer dependency of these as its not _strictly_ required if you're not using TypeScript.
