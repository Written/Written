import type {
  AppwriteContext,
  AppwriteDefault,
  AppwriteResponseObject,
} from '@written/appwrite-types';

export const wrap =
  <T>(wrapper: (ctx: AppwriteContext, rs: AppwriteResponseObject) => T) =>
  (cb: AppwriteDefault): ((ctx: AppwriteContext) => T | Promise<T>) =>
  ctx => {
    const rs = cb(ctx);
    return rs instanceof Promise
      ? rs.then(rs => wrapper(ctx, rs))
      : wrapper(ctx, rs);
  };
export default wrap;
