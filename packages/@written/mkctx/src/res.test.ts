import { ZodError } from 'zod';
import makeResponse from './res';

describe('makeResponse', () => {
  const res = makeResponse();
  describe('res.send()', () => {
    test('Ensure res.send() defaults to the proper values', () =>
      expect(
        res.send('1', 504, {
          a: '1',
        }),
      ).toEqual({
        body: '1',
        statusCode: 504,
        headers: {
          a: '1',
        },
      }));
    // These arent performed by openruntimes but really should be
    test('Ensure res.send() errors for out-of-bounds too-high statuses', () =>
      expect(() => res.send('1', 999)).toThrow(ZodError));
    test('Ensure res.send() errors for out-of-bounds too-low statuses', () =>
      expect(() => res.send('1', -1)).toThrow(ZodError));
  });
  describe('res.empty()', () => {
    test('Ensure res.empty() returns a 204 with no contents & no headers', () =>
      expect(res.empty()).toEqual({
        body: '',
        statusCode: 204,
        headers: {},
      }));
  });
  describe('res.json()', () => {
    test('Ensure res.json() to return proper json', () =>
      expect(
        res.json(
          {
            a: 3,
          },
          200,
        ),
      ).toEqual({
        body: JSON.stringify({ a: 3 }),
        statusCode: 200,
        headers: {
          'content-type': 'application/json',
        },
      }));
    test('Ensure res.json() to persist manually specified headers', () =>
      expect(
        res.json({}, 200, {
          z: '1',
        }).headers,
      ).toEqual({
        'content-type': 'application/json',
        z: '1',
      }));
    test('Ensure res.json() to overwrite content-type', () =>
      expect(
        res.json({}, 200, {
          'content-type': 'not-a/txt',
        }).headers,
      ).toEqual({
        'content-type': 'application/json',
      }));
  });
  describe('res.redirect()', () => {
    test('Ensure res.redirect() defaults to HTTP 301', () =>
      expect(res.redirect('/').statusCode).toEqual(301));
    test('Ensure res.redirect() sets location to URL', () =>
      expect(res.redirect('https://example.com').headers?.location).toEqual(
        'https://example.com',
      ));
    test('Ensure res.redirect() sets location to absolute path', () =>
      expect(res.redirect('/example').headers?.location).toEqual('/example'));
    test('Ensure res.redirect() sets location to relative path', () =>
      expect(res.redirect('../example').headers?.location).toEqual(
        '../example',
      ));
    test('Ensure res.redirect() overwrites location header', () =>
      expect(
        res.redirect('../example', 301, {
          location: '/',
        }).headers?.location,
      ).toEqual('../example'));
    test('Ensure res.redirect() properly handles manually specified statuses (300-399)', () => {
      for (let i = 300; i < 400; i++)
        expect(res.redirect('/', i).statusCode).toEqual(i);
    });
    test('Ensure res.redirect() properly throws on out of bounds (too high)', () =>
      expect(() => res.redirect('/', 400)).toThrow(ZodError));
    test('Ensure res.redirect() properly throws on out of bounds (too low)', () =>
      expect(() => res.redirect('/', 299)).toThrow(ZodError));
  });
});
