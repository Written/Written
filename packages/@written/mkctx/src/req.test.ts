import { AppwriteReq } from '@written/appwrite-types';
import makeRequest from './req';

describe('makeRequest', () => {
  test('Should generate proper object for plain request', () => {
    const requestInfo = 'http://example.com/';
    const requestInit: RequestInit = {};

    const convertedRequest = makeRequest(requestInfo, requestInit);

    const expectedRequest = {
      bodyRaw: '',
      body: undefined,
      headers: {},
      method: 'GET',
      host: 'example.com',
      scheme: 'http',
      query: {},
      queryString: '',
      port: 80,
      url: requestInfo,
      path: '/',
    };

    expect(AppwriteReq.parse(convertedRequest)).toEqual(
      AppwriteReq.parse(expectedRequest),
    );
  });
  describe('Port+Schema Overwriting', () => {
    const portCheck = (scheme: 'http' | 'https', port: number) => {
      test(`Should generate proper object with port overwritten to ${port} on ${scheme}`, () => {
        const rng = Math.random().toString(36);
        const requestInfo = `${scheme}://example.com:${port}/${rng}`;
        const requestInit: RequestInit = {};

        const convertedRequest = makeRequest(requestInfo, requestInit);

        const expectedRequest = {
          bodyRaw: '',
          body: undefined,
          headers: {},
          method: 'GET',
          host: 'example.com',
          scheme,
          query: {},
          queryString: '',
          port,
          url: requestInfo,
          path: `/${rng}`,
        };

        expect(AppwriteReq.parse(convertedRequest)).toEqual(
          AppwriteReq.parse(expectedRequest),
        );
      });
    };
    portCheck('http', 8080);
    portCheck('https', 8443);
    portCheck('http', 443);
    portCheck('https', 80);
  });
  test('Should generate proper object for complex structure', () => {
    const requestInfo = 'https://example.com/path?param1=value1&param2=value2';
    const requestInit: RequestInit = {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        Authorization: 'Bearer Token123',
      },
      body: JSON.stringify({ key: 'value' }),
    };

    const convertedRequest = makeRequest(requestInfo, requestInit);

    const expectedRequest = {
      bodyRaw: requestInit.body,
      body: JSON.parse(requestInit.body as string),
      headers: {
        'content-type': 'application/json',
        authorization: 'Bearer Token123',
      },
      method: 'POST',
      host: 'example.com',
      scheme: 'https',
      query: { param1: 'value1', param2: 'value2' },
      queryString: '?param1=value1&param2=value2',
      port: 443,
      url: requestInfo,
      path: '/path',
    };

    expect(AppwriteReq.parse(convertedRequest)).toEqual(
      AppwriteReq.parse(expectedRequest),
    );
  });
});
