import { AppwriteRes, RedirectCodes } from '@written/appwrite-types';
import { z } from 'zod';

/** A status code */
const StatusCode = z
  .number()
  .min(100, 'Out of bounds status code (Too Low)')
  .max(599, 'Out of bounds status code (Too High)')
  .int();
/** A redirect status code */
const RedirectStatusCode = z
  .number()
  .min(300, 'Out of bounds redirect code (Too Low)')
  .max(399, 'Out of bounds redirect code (Too High)')
  .int();
/** Header type - a string,string record */
const H = z.record(z.string(), z.string());
type H = z.infer<typeof H>;

export class AwRes implements AppwriteRes {
  public send<
    Body extends string,
    Status extends number = 200,
    Headers extends H = {},
  >(
    body: Body,
    statusCode?: Status,
    headers?: Headers,
  ): {
    body: Body;
    statusCode: Status;
    headers: Headers;
  } {
    return {
      body,
      statusCode: StatusCode.parse(statusCode ?? 200) as Status,
      headers: H.parse(headers ?? {}) as Headers,
    };
  }
  public json<Status extends number, Headers extends H = {}>(
    obj: Record<any, any>,
    status?: Status,
    headers?: Headers,
  ): {
    body: string;
    statusCode: Status;
    headers: Headers & {
      'content-type': 'application/json';
    };
  } {
    return this.send(JSON.stringify(obj), status, {
      ...(headers ?? ({} as never)),
      'content-type': 'application/json',
    });
  }
  public empty() {
    return this.send('', 204, {});
  }
  public redirect<
    Target extends string,
    Status extends number = typeof RedirectCodes.MovedPermanently,
    Headers extends H = {},
  >(
    url: Target,
    status?: Status,
    headers?: H,
  ): {
    body: '';
    statusCode: Status;
    headers: Headers & {
      location: Target;
    };
  } {
    return this.send(
      '',
      RedirectStatusCode.parse(
        status ?? RedirectCodes.MovedPermanently,
      ) as Status,
      {
        ...((headers ?? {}) as Headers),
        location: url,
      },
    ) as any;
  }
}

export const makeResponse = () => new AwRes();
export default makeResponse;
