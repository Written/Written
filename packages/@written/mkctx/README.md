<div align="center">

![Written Logo](https://codeberg.org/Written/Written/raw/branch/senpai/ico/written.png)

# @written/mkctx

Generate Appwrite Context Objects. Useful for testing & making non-appwrite-runner calls to appwrite functions.

See also: [@written/httpcompat](https://codeberg.org/Written/Written/src/branch/senpai/packages/@written/httpcompat)

[![Codeberg](https://img.shields.io/badge/Codeberg-Written-724773.svg?style=flat-square)](https://codeberg.org/Written/Written/src/branch/senpai/packages/@written/mkctx)
[![License](https://img.shields.io/badge/License-MIT-724773.svg?style=flat-square)](https://codeberg.org/Written/Written/src/branch/senpai/LICENSE)

</div>

## Installation

```sh
pnpm i @written/mkctx
```

## Usage

The `ctx` constructor takes the same args as `fetch()`.<br/>
Example:

```ts
import Ctx from '@written/mkctx';
const ctx = new Ctx('https://example.com/test', {
  method: 'POST',
});

// ...

(async () => {
  const response = await import('./someAppwriteFunc').default(ctx);
  // todo: handle response
})();
```

### Obtaining Logs

You can use `Ctx.getLogs(ctx)`/`Ctx.getErrors(ctx)` to get the logs from the context object.
