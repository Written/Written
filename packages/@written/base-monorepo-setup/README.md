<div align="center">

![Written Logo](https://codeberg.org/Written/Written/raw/branch/senpai/ico/written.png)

# @written/base-monorepo-setup

Utility Function for creating a baseline monorepo using nx within create- scripts.

[![Codeberg](https://img.shields.io/badge/Codeberg-Written-724773.svg?style=flat-square)](https://codeberg.org/Written/Written/src/branch/senpai/packages/@written/base-monorepo-setup)
[![License](https://img.shields.io/badge/License-MIT-724773.svg?style=flat-square)](https://codeberg.org/Written/Written/src/branch/senpai/LICENSE)

</div>

## Installation

In your project, run:

```sh
pnpm i -D @written/base-monorepo-setup
```

## Usage

See [Written@`9eff97d`/packages/create-written/src/variants/project.ts](https://codeberg.org/Expo/Written/src/commit/9eff97ddfaafddb674448ec515e920b951aeda1b/packages/create-written/src/variants/project.ts#L104-L113) for example usage.

TODO: Add usage instructions.
