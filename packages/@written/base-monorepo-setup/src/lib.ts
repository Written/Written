import path from 'path';
import { ensureDirSync } from '@3xpo/fs-extra';
import fs from 'fs/promises';
import { exec } from 'child_process';

export type Task = {
  /**
   * Task title
   */
  title: string;
  /**
   * Task function
   */
  task: (
    message: (string: string) => void,
  ) => string | Promise<string> | void | Promise<void>;
  /**
   * If enabled === false the task will be skipped
   */
  enabled?: boolean;
};

/** Creates a baseline monorepo; see {@link https://codeberg.org/Expo/Written/src/commit/9eff97ddfaafddb674448ec515e920b951aeda1b/packages/create-written/src/variants/project.ts#L104-L113 this} for an example on how to use it */
export const setupBaseMonorepo = async (opt: {
  /** Which directory to create everything in */
  dir: string;
  /** The list of paths to use in the workspace for packages - e.g. `["packages/*"]` */
  workspacePaths: string[];
  /** If we should init git */
  git: boolean;
  /** List of dependencies we need at the root level */
  rootDependencies: string[];
  /** The package.json to use - we'll add some utility fields (dev, build, test) if none are provided. */
  packageJSON: Record<string, any>;
  /** The nx config */
  nx?: Record<string, any>;
  /** The Tasks Execution Func - simply needs to run all tasks' .task() function. See {@link https://codeberg.org/Expo/Written/src/commit/f155051056a050fe9fc46578b86b619b403eaf07/packages/create-written/src/util/Task.ts#L23-L42 this implementation} for reference on how to do this with a TUI */
  tasks?: (tasks: Task[]) => Promise<void>;
}) => {
  // #region Utility Functions
  const { dir, rootDependencies, packageJSON } = opt;
  packageJSON.private = true;
  const nx = {
    $schema: './node_modules/nx/schemas/nx-schema.json',
    extends: 'nx/presets/npm.json',
    neverConnectToCloud: true,
    ...(opt.nx ?? {}),
    affected: {
      defaultBase: 'senpai',
      ...(opt.nx?.defaultBase ?? {}),
    },
    targetDefaults: {
      build: {
        cache: true,
        dependsOn: ['^build'],
      },
      lint: {
        cache: true,
      },
      e2e: {
        cache: true,
      },
      ...(opt.nx?.targetDefaults ?? {}),
    },
  };
  const tasks =
    opt.tasks ??
    (async (tasks: Task[]) => {
      for (const task of tasks) {
        if (task.enabled === false) continue;
        try {
          await task.task(() => void 0);
        } catch (error) {
          console.error(error);
          process.exit(1);
        }
      }
    });
  ensureDirSync(dir);
  const task = (
    taskName: string,
    task: Parameters<typeof tasks>[0][number]['task'],
    enabled: Parameters<typeof tasks>[0][number]['enabled'] = true,
  ) =>
    tasks([
      {
        title: taskName,
        task,
        enabled,
      },
    ]);
  const run = (cmd: string) =>
    new Promise((rs, rj) => {
      exec(cmd, {
        cwd: dir,
      })
        .on('exit', e =>
          e === 0
            ? rs(e)
            : rj('Command ' + JSON.stringify(cmd + ' exited with ' + e)),
        )
        .on('error', rj);
    });
  // #endregion
  // #region Configuration
  await task(
    'Create Package.json',
    () =>
      fs
        .writeFile(
          path.join(dir, 'package.json'),
          JSON.stringify(packageJSON, null, 2),
        )
        .then(() => 'Created Package.json') as Promise<string>,
  );
  await task(
    'Create pnpm configuration',
    () =>
      Promise.all([
        fs.writeFile(
          path.join(dir, '.npmrc'),
          `strict-peer-dependencies=true
auto-install-peers=false
node-linker=hoisted
hoist-pattern=*
public-hoist-pattern=*
shamefully-hoist=true
engine-strict=true
prefer-workspace-packages=true
shared-workspace-lockfile=true
dedupe-direct-deps=true
lockfile-include-tarball-url=true
package-import-method=hardlink
use-node-version=${process.versions.node} # The node version used at build time
  `,
        ),
        fs.writeFile(
          path.join(dir, 'pnpm-workspace.yaml'),
          `packages:
${opt.workspacePaths.map(v => `- ${v}`).join('\n')}
`,
        ),
      ]).then(() => 'Created pnpm Configuration Files') as Promise<string>,
  );
  await task('Create Gitignore', async () => {
    await fs.writeFile(
      path.join(dir, '.gitignore'),
      `node_modules
dist
.sass-cache
*.lock
*.log
/typings
.DS_Store
Thumbs.db
.nx/cache
.env
.prettiercache
*.bak
*.ignoreme
`,
    );
  });
  // #endregion
  await tasks([
    {
      title: 'Install Dependencies',
      async task() {
        await run(
          `pnpm i -Dw ${rootDependencies.map(v => JSON.stringify(v)).join(' ')}`,
        );
        return 'Installed Dependencies';
      },
    },
    {
      title: 'Create Workspace',
      async task() {
        await fs.writeFile(path.join(dir, 'nx.json'), JSON.stringify(nx));
        return 'Created Workspace';
      },
    },
    {
      enabled: opt.git,
      title: 'Create Git Repository',
      async task() {
        await run('git init --initial-branch senpai');
        return 'Created Git Repository';
      },
    },
  ]);
  await task(
    'Setup nx',
    () =>
      run('pnpm nx reset').then(_ => 'Completed nx Setup') as Promise<string>,
  );
  ensureDirSync(dir + '/');
  await tasks([
    {
      title: 'Install Dependencies',
      async task() {
        await run(
          `pnpm i -Dw ${rootDependencies.map(v => JSON.stringify(v)).join(' ')}`,
        );
        return 'Installed Dependencies';
      },
    },
  ]);
};
export default setupBaseMonorepo;
