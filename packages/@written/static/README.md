<div align="center">

![Written Logo](https://codeberg.org/Written/Written/raw/branch/senpai/ico/written.png)

# @written/static

Static file serving middleware for Written.

[![Codeberg](https://img.shields.io/badge/Codeberg-Written-724773.svg?style=flat-square)](https://codeberg.org/Written/Written/src/branch/senpai/packages/@written/static)
[![License](https://img.shields.io/badge/License-MIT-724773.svg?style=flat-square)](https://codeberg.org/Written/Written/src/branch/senpai/LICENSE)

</div>

## Installation

In your function, run:

```sh
pnpm i @written/static
```

## Usage

```ts
import App from '@written/app';
import static from '@written/static';

const app = new App();

app.use(
  static({
    directory: `${__dirname}/../`, // Additional optional options can be found in the type declaration
  }),
);

export default app.server();
```
