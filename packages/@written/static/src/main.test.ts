import App, { ServerMode } from '@written/app';
import Static from './main.ts';
import Ctx from '@written/mkctx';

describe('@written/static', () => {
  let app: App;
  beforeEach(() => {
    app = new App();
  });
  test('Load Middleware', () => {
    app.use(
      Static({
        directory: __dirname + '/..',
      }),
    );
    app.server(ServerMode.Appwrite);
  });
  test('Trigger Middleware', () => {
    app.use(
      Static({
        directory: __dirname + '/..',
      }),
    );
    app.server(ServerMode.Appwrite)(new Ctx('https://localhost/README.md'));
  });
  test('Have a default cache ttl of 3 days', async () => {
    app.use(
      Static({
        directory: __dirname + '/..',
      }),
    );
    const desiredCache = 'public, max-age=259200';
    const res = await app.server(ServerMode.Appwrite)(
      new Ctx('https://localhost/README.md'),
    );
    expect(res.headers?.['cache-control']).toEqual(desiredCache);
  });
  test('Resolve the README', async () => {
    app.use(
      Static({
        directory: __dirname + '/..',
      }),
    );
    const res = await app.server(ServerMode.Appwrite)(
      new Ctx('https://localhost/README.md'),
    );

    expect(res.body.toString()).toContain('@written/static');
    expect(res.statusCode).toEqual(200);
  });
  test('Resolve the index file', async () => {
    app.use(
      Static({
        directory: __dirname + '/..',
        index: 'README.md',
      }),
    );
    const res = await app.server(ServerMode.Appwrite)(
      new Ctx('https://localhost/'),
    );

    expect(res.body.toString()).toContain('@written/static');
    expect(res.statusCode).toEqual(200);
  });
  test('Respond with the correct MIME Type', async () => {
    app.use(
      Static({
        directory: __dirname + '/..',
      }),
    );
    const res = await app.server(ServerMode.Appwrite)(
      new Ctx('https://localhost/README.md'),
    );

    expect(res.headers?.['content-type']).toEqual('text/markdown');
  });
  test('Disallow .env', async () => {
    app.use(
      Static({
        directory: __dirname + '/..',
      }),
    );
    const res = await app.server(ServerMode.Appwrite)(
      new Ctx('https://localhost/.env'),
    );

    expect(res.statusCode).toEqual(403);
  });
});
