<div align="center">

![Written Logo](https://codeberg.org/Written/Written/raw/branch/senpai/ico/written.png)

# @written/cors-setup

Setup CORS to, by default, allow everything, in your Appwrite app. Offers a wrapper to use with [`@written/wrap`](https://npm.im/@written/wrap), exported as `wrapCorsResponse`, aswell as a function to, based on the inputted args, return just the response headers.

[![Codeberg](https://img.shields.io/badge/Codeberg-Written-724773.svg?style=flat-square)](https://codeberg.org/Written/Written/src/branch/senpai/packages/@written/cors-setup)
[![License](https://img.shields.io/badge/License-MIT-724773.svg?style=flat-square)](https://codeberg.org/Written/Written/src/branch/senpai/LICENSE)

</div>

## Installation

In your project, run:

```sh
pnpm i @written/cors-setup
```

## Usage

TODO: Describe usage.

## Notice

This is a mostly internal package, it's recommended to directly use [`@written/app`](https://npm.im/@written/app)'s `app.cors()` method, which calls this.
