import { AppwriteReq } from '@written/appwrite-types';
import HttpCompat from './lib';
import Ctx from '@written/mkctx';
describe('@written/httpcompat', () => {
  const mockAppwriteRequest: AppwriteReq = new Ctx('https://example.com/api', {
    body: 'test body',
    method: 'POST',
    headers: {
      'content-type': 'text/plain;charset=UTF-8',
    },
  }).req;

  const mockHttpResponse = new Response('test response', {
    headers: { 'Content-Type': 'text/plain' },
    status: 200,
  });

  const mockAppwriteResponse = {
    headers: { 'Content-Type': 'text/plain' },
    statusCode: 200,
    body: new ArrayBuffer(8),
  };

  test('bufferIsBinary should correctly identify binary data', async () => {
    const textData = 'This is a text string.';
    const binaryData = new Uint8Array([0x48, 0x65, 0x6c, 0x6c, 0x6f]);

    expect(HttpCompat.bufferIsBinary(Buffer.from(textData))).toBe(false);
    expect(HttpCompat.bufferIsBinary(binaryData.buffer)).toBe(false);

    expect(
      HttpCompat.bufferIsBinary(
        await fetch('https://codeberg.org/assets/img/favicon.png').then(v =>
          v.arrayBuffer(),
        ),
      ),
    ).toBe(true);
  });

  test('tryJSONParse should parse JSON or fallback to the original value', () => {
    const jsonString = '{"key": "value"}';
    const nonJsonString = 'Not a JSON string';

    expect(HttpCompat['tryJSONParse'](jsonString)).toEqual({ key: 'value' });
    expect(HttpCompat['tryJSONParse'](nonJsonString)).toBe(nonJsonString);
    expect(HttpCompat['tryJSONParse'](nonJsonString, 'fallback')).toBe(
      'fallback',
    );
  });

  test('appwriteRequestToHttpRequest should convert Appwrite request to HTTP request', async () => {
    const httpRequest =
      HttpCompat.appwriteRequestToHttpRequest(mockAppwriteRequest);
    const bodyDecoded = await httpRequest.text();

    expect(httpRequest.url).toBe(mockAppwriteRequest.url);
    expect(httpRequest.method).toBe(mockAppwriteRequest.method);
    expect(bodyDecoded).toBe(mockAppwriteRequest.bodyRaw);
    expect(httpRequest.headers.get('Content-Type')).toBe(
      'text/plain;charset=UTF-8',
    );
  });

  test('appwriteResponseToHttpResponse should convert Appwrite response to HTTP response', () => {
    const httpResponse =
      HttpCompat.appwriteResponseToHttpResponse(mockAppwriteResponse);

    expect(httpResponse.status).toBe(mockAppwriteResponse.statusCode);
    expect(httpResponse.headers.get('Content-Type')).toBe('text/plain');
    // @ts-ignore idk why its complaining about not being able to find ReadableStream
    expect(httpResponse.body).toBeInstanceOf(ReadableStream);
  });

  test('httpRequestToAppwriteRequest should convert HTTP request to Appwrite request', async () => {
    const httpRequest = new Request(mockAppwriteRequest.url, {
      method: mockAppwriteRequest.method,
      body: mockAppwriteRequest.bodyRaw,
      headers: mockAppwriteRequest.headers,
    });

    const appwriteRequest =
      await HttpCompat.httpRequestToAppwriteRequest(httpRequest);

    expect(appwriteRequest.url).toBe(mockAppwriteRequest.url);
    expect(appwriteRequest.method).toBe(mockAppwriteRequest.method);
    expect(appwriteRequest.bodyRaw).toBe(mockAppwriteRequest.bodyRaw);
    expect(appwriteRequest.headers).toEqual(mockAppwriteRequest.headers);
  });

  test('httpResponseToAppwriteResponse should convert HTTP response to Appwrite response', async () => {
    const appwriteResponse =
      await HttpCompat.httpResponseToAppwriteResponse(mockHttpResponse);

    expect(appwriteResponse.statusCode).toBe(mockHttpResponse.status);
    expect(appwriteResponse.headers).toEqual({ 'content-type': 'text/plain' });
    expect(typeof appwriteResponse.body).toEqual('string');
  });
});
