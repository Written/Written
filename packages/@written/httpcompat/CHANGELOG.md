# @written/httpcompat

## 0.1.17

### Patch Changes

- a748109: Update Deps
- Updated dependencies [a748109]
  - @written/appwrite-types@0.2.17

## 0.1.16

### Patch Changes

- 51d9506: Upgrade Dependencies
- Updated dependencies [51d9506]
  - @written/appwrite-types@0.2.16

## 0.1.15

### Patch Changes

- d1ea241: Update Deps
- Updated dependencies [d1ea241]
  - @written/appwrite-types@0.2.15

## 0.1.14

### Patch Changes

- Updated dependencies [1df6d84]
  - @written/appwrite-types@0.2.14

## 0.1.13

### Patch Changes

- 4fa1758: Upgrade dependencise
- 034c27f: Mark the headers object as an AppwriteRequestHeaders object
- a0c9930: Upgrade Dependencies
- Updated dependencies [034c27f]
- Updated dependencies [4fa1758]
- Updated dependencies [39ce77a]
- Updated dependencies [a0c9930]
  - @written/appwrite-types@0.2.13

## 0.1.12

### Patch Changes

- Updated dependencies [816cec3]
  - @written/appwrite-types@0.2.12

## 0.1.11

### Patch Changes

- 3872f26: Add LICENSE files
- bee1c23: Move repos to Written/Written
- de25848: Consistent, Clean README header format
- Updated dependencies [8acd2e0]
- Updated dependencies [bee1c23]
- Updated dependencies [de25848]
  - @written/appwrite-types@0.2.11

## 0.1.10

### Patch Changes

- 40f46fa: Upgrade Dependencies
- Updated dependencies [40f46fa]
  - @written/appwrite-types@0.2.10

## 0.1.9

### Patch Changes

- 4a5dd4d: bump dependencies
- Updated dependencies [4a5dd4d]
  - @written/appwrite-types@0.2.9

## 0.1.8

### Patch Changes

- rebuild everything due to a filesystem error during the last build
- Updated dependencies
  - @written/appwrite-types@0.2.8

## 0.1.7

### Patch Changes

- cc4fcee: Use the ~ version range

## 0.1.6

### Patch Changes

- bump everything
- Updated dependencies
  - @written/appwrite-types@0.2.7

## 0.1.5

### Patch Changes

- 18c6dd1: fix: use es6 in lib, i hate you typescript
- Updated dependencies [18c6dd1]
  - @written/appwrite-types@0.2.6

## 0.1.4

### Patch Changes

- 48bc017: Upgrade Dependencies

## 0.1.3

### Patch Changes

- Updated dependencies [1208d0d]
  - @written/appwrite-types@0.2.5

## 0.1.2

### Patch Changes

- Updated dependencies [bca8b56]
  - @written/appwrite-types@0.2.4
