<div align="center">

![Written Logo](https://codeberg.org/Written/Written/raw/branch/senpai/ico/written.png)

# @written/httpcompat

Convert Appwrite Requests/Responses from/to HTTP Requests/Responses. Useful for calling some code not built for Appwrite or calling Appwrite Functions from non-Appwrite-function codebases.

[![Codeberg](https://img.shields.io/badge/Codeberg-Written-724773.svg?style=flat-square)](https://codeberg.org/Written/Written/src/branch/senpai/packages/@written/httpcompat)
[![License](https://img.shields.io/badge/License-MIT-724773.svg?style=flat-square)](https://codeberg.org/Written/Written/src/branch/senpai/LICENSE)

</div>

## Installation

In your project, run:

```sh
pnpm i @written/httpcompat
```

## Usage

TODO: Add usage instructions.
