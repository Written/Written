export default (id: string, funcName: string, description: string) => `/**
* @id ${id}
* @name ${funcName}
* @description ${description.split('\n').join(`
* `)}
*/

import 'source-map-support/register';

// Function Contents
import { Client } from 'node-appwrite';
import App from '@written/app';
import { z } from 'zod';

const app = new App().cors();

const env = z
  .object({
    APPWRITE_FUNCTION_ENDPOINT: z.string(),
    APPWRITE_FUNCTION_PROJECT_ID: z.string(),
    APPWRITE_API_KEY: z.string(),
    NODE_ENV: z.string().default('production'),
  })
  .parse(process.env);

const client = new Client()
  .setEndpoint(env.APPWRITE_FUNCTION_ENDPOINT)
  .setProject(env.APPWRITE_FUNCTION_PROJECT_ID)
  .setKey(env.APPWRITE_API_KEY);
 
app.get(null, async ({ req, res, log, error }) => { // null = any route
  log('Hello, Logs!');
  error('Hello, Errors!');
  return res.send('Hello, World!');
});
app.use(({res}) => {
  // \`res.json()\` is a handy helper for sending JSON
  return res.json({
    motto: 'Build like a team of hundreds_',
    learn: 'https://appwrite.io/docs',
    connect: 'https://appwrite.io/discord',
    getInspired: 'https://builtwith.appwrite.io',
  });
});

export default app.server();
`;
