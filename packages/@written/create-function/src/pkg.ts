export default (displayName: string, id: string, description: string) => ({
  name: id,
  displayName,
  version: '1.0.0',
  description,
  main: 'dist/main.js',
  source: 'src/main.ts',
  type: 'commonjs',
  scripts: {
    build: 'aw-fn build',
    dev: 'aw-fn watch',
    deploy: 'aw-fn deploy',
    lint: 'prettier -c .',
    format: 'prettier -c -w .',
    'aw-deploy': 'aw-fn deploy',
  } as Record<string, string>,
  private: true,
});
