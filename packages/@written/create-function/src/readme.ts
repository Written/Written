export default (
  name: string,
  description: string,
  includeExampleRoute: boolean,
  runtime = 'node-20.0',
) => `# ${name}

${description}

## 🧰 Usage

### GET /

- ${includeExampleRoute ? 'Returns a "Hello, World!" message.' : 'TODO: Add Description'}

**Response**

Sample \`200\` Response:

${
  includeExampleRoute
    ? `\`\`\`text
Hello, World!
\`\`\``
    : 'TODO: Add Response Documentation Here'
}
${
  includeExampleRoute
    ? `
### POST, PUT, PATCH, DELETE /

- Returns a "Learn More" JSON response.

**Response**

Sample \`200\` Response:

\`\`\`json
{
  "motto": "Build like a team of hundreds_",
  "learn": "https://appwrite.io/docs",
  "connect": "https://appwrite.io/discord",
  "getInspired": "https://builtwith.appwrite.io"
}
\`\`\``
    : ''
}

## ⚙️ Configuration

| Setting           | Value         |
|-------------------|---------------|
| Runtime           | ${runtime.charAt(0).toUpperCase() + runtime.split('-')[0].substring(1)} (${runtime.split('-')[1]})   |
| Entrypoint        | \`src/main.js\` |
| Build Commands    | \`npm install\` |
| Permissions       | \`any\`         |
| Timeout (Seconds) | 15            |

## 🔒 Environment Variables

No function-specific environment variables required.
`;
