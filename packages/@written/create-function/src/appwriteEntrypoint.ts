export default (id: string, funcName: string, description: string) => `/**
* @id ${id}
* @name ${funcName}
* @description ${description.split('\n').join(`
* `)}
*/

import 'source-map-support/register';

// Function Contents
import { Client } from 'node-appwrite';
import defineHandler from '@awfn/define-handler';
import { z } from 'zod';

const app = new App().cors();

const env = z
 .object({
   APPWRITE_FUNCTION_ENDPOINT: z.string(),
   APPWRITE_FUNCTION_PROJECT_ID: z.string(),
   APPWRITE_API_KEY: z.string(),
   NODE_ENV: z.string().default('production'),
 })
 .parse(process.env);

const client = new Client()
  .setEndpoint(env.APPWRITE_FUNCTION_ENDPOINT)
  .setProject(env.APPWRITE_FUNCTION_PROJECT_ID)
  .setKey(env.APPWRITE_API_KEY);

export default defineHandler(({req,res})=>{
  return res.json({
    success: true,
    message: 'Hello, World!',
  });
});
`;
