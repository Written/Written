#!/usr/bin/env node
import consola from 'consola';
import slug from './slug';
import fs from 'fs';
import basePkg from './pkg';
import path from 'path';
import { execSync } from 'child_process';
import readme from './readme';
import appwriteEntrypoint from './appwriteEntrypoint';
import writtenEntrypoint from './writtenEntrypoint';

const runtime = 'node-20.0';

(async () => {
  if (!fs.existsSync(path.resolve(process.cwd(), 'appwrite.json'))) {
    if (
      !fs.existsSync(
        path.resolve(process.cwd(), 'packages', 'appwrite', 'appwrite.json'),
      )
    )
      throw new Error('No Appwrite Config! Cannot proceed.');
    else process.chdir(path.resolve(process.cwd(), 'packages', 'appwrite'));
  }
  const name = (
    await consola.prompt('What do you want to name your function?', {
      required: true,
      type: 'text',
    })
  ).trim();
  const id = (
    await consola.prompt("What do you want as your function's id?", {
      required: true,
      type: 'text',
      default: slug(name),
      placeholder: slug(name),
    })
  ).trim();
  if (id !== slug(id))
    return consola.error(
      new Error(
        'invalid id, must only include a-z, 0-9 and hypens - try ' +
          JSON.stringify(slug(id)),
      ),
    );
  const desc = await consola.prompt('How would you describe your function?', {
    required: true,
    type: 'text',
    default: 'No Description Yet.',
    placeholder: 'No Description Yet.',
  });
  const plainAppwrite = fs.existsSync('.use-plain-appwrite');
  const pkg = basePkg(name, id, desc);
  const dir = path.resolve(process.cwd(), 'functions', id);
  fs.mkdirSync(path.join(dir, 'src'), { recursive: true });
  fs.writeFileSync(
    path.join(dir, 'package.json'),
    JSON.stringify(pkg, null, 2),
  );
  consola.info('Updating appwrite.json');
  const aw = JSON.parse(fs.readFileSync('appwrite.json', 'utf-8'));
  aw.functions = (aw.functions ?? []).filter(
    (v: { $id: string }) => v.$id !== id,
  );
  aw.functions.push({
    $id: id,
    name: name,
    runtime,
    execute: ['any'],
    schedule: '',
    timeout: 15,
    enabled: true,
    logging: true,
    entrypoint: pkg.main,
    commands: `npm install --omit=dev && npm run appwrite:build --if-present`,
    ignore: ['node_modules', '.npm'],
    path: path.relative(process.cwd(), dir),
  });
  fs.writeFileSync('appwrite.json', JSON.stringify(aw, null, 2));
  consola.info('Setting up README.md');
  fs.writeFileSync(
    path.resolve(dir, 'README.md'),
    readme(name, desc, !plainAppwrite, runtime),
  );
  consola.info('Setting up ' + pkg.source);
  fs.writeFileSync(
    path.resolve(dir, pkg.source),
    plainAppwrite
      ? appwriteEntrypoint(id, name, desc)
      : writtenEntrypoint(id, name, desc),
  );
  consola.info('Installing Dependencies');
  execSync(`pnpm i node-appwrite source-map-support`, {
    cwd: dir,
    stdio: 'inherit',
  });
  consola.info('Installing Dev Dependencies');
  execSync(
    `pnpm i -D @awfn/cli esbuild prettier @types/source-map-support zod ${plainAppwrite ? '@awfn/define-handler' : '@written/app'}${
      fs.existsSync('.create-func-deps')
        ? ' ' +
          fs
            .readFileSync('.create-func-deps', 'utf-8')
            .split('\n')
            .map(v => v.trim())
            .filter(v => v)
        : ''
    }`,
    {
      cwd: dir,
      stdio: 'inherit',
    },
  );
})();
