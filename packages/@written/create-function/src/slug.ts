export default (str: string) =>
  str
    .toLowerCase()
    .replace(/[^a-z0-9-]/giu, '-')
    .replace(/-+/giu, '-');
