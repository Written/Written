# @written/create-function

## 0.1.11

### Patch Changes

- a748109: Update Deps

## 0.1.10

### Patch Changes

- 51d9506: Upgrade Dependencies

## 0.1.9

### Patch Changes

- 93b4f20: Change how commands are passed

## 0.1.8

### Patch Changes

- d1ea241: Update Deps

## 0.1.7

### Patch Changes

- 4fa1758: Upgrade dependencise
- a0c9930: Upgrade Dependencies

## 0.1.6

### Patch Changes

- 3872f26: Add LICENSE files
- bee1c23: Move repos to Written/Written
- de25848: Consistent, Clean README header format

## 0.1.5

### Patch Changes

- 40f46fa: Upgrade Dependencies

## 0.1.4

### Patch Changes

- 4a5dd4d: bump dependencies
- b5ec69e: mention appwrite needs config node 20

## 0.1.3

### Patch Changes

- 02afafb: use node-20 instead of just plain 20

## 0.1.2

### Patch Changes

- rebuild everything due to a filesystem error during the last build

## 0.1.1

### Patch Changes

- bump everything
