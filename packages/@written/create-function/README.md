<div align="center">

![Written Logo](https://codeberg.org/Written/Written/raw/branch/senpai/ico/written.png)

# @written/create-function

Creates an Appwrite/Written function

[![Codeberg](https://img.shields.io/badge/Codeberg-Written-724773.svg?style=flat-square)](https://codeberg.org/Written/Written/src/branch/senpai/packages/@written/create-function)
[![License](https://img.shields.io/badge/License-MIT-724773.svg?style=flat-square)](https://codeberg.org/Written/Written/src/branch/senpai/LICENSE)

</div>

## Usage

```sh
pnpm create @written/function
```

TODO: Add further usage instructions.

## Notice

The function it creates defaults to Node 20 - Make sure your appwrite is configured to use that.
