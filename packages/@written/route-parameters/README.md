<div align="center">

![Written Logo](https://codeberg.org/Written/Written/raw/branch/senpai/ico/written.png)

# @written/route-parameters

TypeScript type for route parameters, extracted from a route string. Heavily based on @types/express' type.

[![Codeberg](https://img.shields.io/badge/Codeberg-Written-724773.svg?style=flat-square)](https://codeberg.org/Written/Written/src/branch/senpai/packages/@written/route-parameters)
[![License](https://img.shields.io/badge/License-MIT-724773.svg?style=flat-square)](https://codeberg.org/Written/Written/src/branch/senpai/LICENSE)

</div>

## Installation

In your project, run:

```sh
pnpm i @written/route-parameters -D
```

## Usage

```ts
import type { RouteParameters } from '@written/route-parameters';

type Params = RouteParameters<'/test/:abc'>; // = {abc:string}
```
