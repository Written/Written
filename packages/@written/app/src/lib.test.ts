import { AppwriteResponseObject } from '@written/appwrite-types';
import App from './lib';
import Ctx from '@written/mkctx';
import Trailing from '@written/trailing-slash';

describe('new App()', () => {
  describe('Appwrite Mode', () => {
    let app: App;
    let fetch: (
      requestInfo: string,
      requestInit?: RequestInit,
    ) => AppwriteResponseObject | Promise<AppwriteResponseObject> = () => {
      throw new Error(`Didn't init fetch`);
    };
    beforeEach(() => {
      app = new App();
      const server = app.server();
      fetch = (requestInfo: string, requestInit: RequestInit = {}) =>
        server(new Ctx(`https://example.com${requestInfo}`, requestInit));
    });
    describe('Sanity', () => {
      it('should return the correct body', async () => {
        app.use(({ res }) => res.send('Hello, World!'));
        expect((await fetch('/')).body).toEqual('Hello, World!');
      });
      it('should return 200 by default', async () => {
        app.use(({ res }) => res.send('Hello, World!'));
        expect((await fetch('/')).statusCode).toEqual(200);
      });
      it('should allow properly overwriting the status code', async () => {
        app.use(({ res }) => res.send('Hello, World!', 300));
        expect((await fetch('/')).statusCode).toEqual(300);
      });
      it('should allow specifying headers', async () => {
        app.use(({ res }) =>
          res.send('', 200, {
            a: 'b',
          }),
        );
        expect((await fetch('/')).headers['a']).toEqual('b');
      });
      it('should return proper json', async () => {
        app.use(({ res }) =>
          res.json({
            test: 1,
          }),
        );
        const body = (await fetch('/')).body;
        expect(body).toEqual(JSON.stringify({ test: 1 }));
        expect(JSON.parse(body)).toEqual({ test: 1 });
      });
      it('should return a proper empty response', async () => {
        app.use(({ res }) => res.empty());
        const res = await fetch('/');
        expect(res.body).toEqual('');
        expect(res.statusCode).toEqual(204);
      });
    });
    describe('Middleware Implementaiton', () => {
      it('should run middleware in order', async () => {
        app.use(({ locals, next }) => {
          locals.abc = 1;
          return next();
        });
        app.use(({ locals, next }) => {
          locals.abc++;
          return next();
        });
        app.use(({ locals, next }) => {
          const rs = next();
          locals.abc++;
          return rs;
        });
        app.use(({ locals, res }) => res.send(`${locals.abc}`));
        app.use(({ locals, next }) => {
          locals.abc++;
          return next();
        });
        expect((await fetch('/')).body).toEqual('2');
      });
      it('should run middleware based on method', async () => {
        app.use(({ locals, next }) => {
          locals.abc = 1;
          return next();
        });
        app.post(null, ({ locals, next }) => {
          locals.abc++;
          return next();
        });
        app.use(({ locals, next }) => {
          const rs = next();
          locals.abc++;
          return rs;
        });
        app.get(null, ({ locals, res }) => res.send(`${locals.abc}`));
        app.use(({ locals, next }) => {
          locals.abc++;
          return next();
        });
        expect((await fetch('/')).body).toEqual('1');
      });
    });
    describe('Routing', () => {
      it('should resolve basic constant paths', async () => {
        app.get('/', ({ res }) => res.send('hi from /'));
        app.get('/a', ({ res }) => res.send('hi from /a'));
        app.get('/b', ({ res }) => res.send('hi from /b'));
        app.get('/c', ({ res }) => res.send('hi from /c'));

        let res = await fetch('/a');
        expect(res.body).toEqual('hi from /a');
        res = await fetch('/b');
        expect(res.body).toEqual('hi from /b');
        res = await fetch('/c');
        expect(res.body).toEqual('hi from /c');
        res = await fetch('/');
        expect(res.body).toEqual('hi from /');
      });
      it('should resolve fancy routes with params', async () => {
        app.get('/', ({ res }) => res.send('hi from /'));
        app.get('/paginated/', ({ res }) => res.send('hi from /paginated/'));
        app.get('/paginated/:page', ({ res, params }) =>
          res.send(`hi from special route with param ${params.page}`),
        );

        let res = await fetch('/paginated/');
        expect(res.body).toEqual('hi from /paginated/');
        res = await fetch('/paginated/4');
        expect(res.body).toEqual('hi from special route with param 4');
      });
      it('should resolve query parameters', async () => {
        app.get('/?a', ({ res }) => res.send('hi from /?a'));

        let res = await fetch('/?a');
        expect(res.body).toEqual('hi from /?a');
      });
      it('should resolve query parameters not in path matcher', async () => {
        app.get('/', ({ res }) => res.send('hi from /'));

        let res = await fetch('/?a');
        expect(res.body).toEqual('hi from /');
      });
      it('should resolve query parameters when using params', async () => {
        app.get('/?:a', ({ res, params }) => res.send('hi from /?' + params.a));
        app.get('/tomato?:a', ({ res, params }) =>
          res.send('hi from /tomato?' + params.a),
        );

        let res = await fetch('/?gz');
        expect(res.body).toEqual('hi from /?gz');
        res = await fetch('/tomato/?gz');
        expect(res.body).toEqual('hi from /tomato?gz');
      });
      it('should resolve query parameters and normal params when using params', async () => {
        app.get<['b', 'a']>('/some/path/to/:b?:a', ({ res, params }) =>
          res.send('henlo u passed b=' + params.b + ' & a=' + params.a),
        );

        let res = await fetch('/some/path/to/test?urmom');
        expect(res.body).toEqual('henlo u passed b=test & a=urmom');
        res = await fetch('/some/path/to/test/?urmom');
        expect(res.body).toEqual('henlo u passed b=test & a=urmom');
      });
      it('should properly handle trailing slashes', async () => {
        app.get('/test', ({ res }) => res.send('hi from /test'));

        const res = await fetch('/test/');
        expect(res.body).toEqual('hi from /test');
      });
      it('should properly handle trailing slashes even with params', async () => {
        app.get('/test/:a/b', ({ res }) => res.send('hi from /test/:a/b'));

        const res = await fetch('/test/x/b/');
        expect(res.body).toEqual('hi from /test/:a/b');
      });
      it('should properly handle trailing slashes even with params at the end', async () => {
        app.get('/test/:a/', ({ res, params }) =>
          res.send('hi from /test/:a, your param was ' + params.a),
        );

        const res = await fetch('/test/x');
        expect(res.body).toEqual('hi from /test/:a, your param was x');
      });
      it('should properly handle trailing slashes when set to always', async () => {
        app.config.routingTrailingSlashes = Trailing.Mode.Always;

        app.get('/test/:a/', ({ res, params }) =>
          res.send('hi from /test/:a, your param was ' + params.a),
        );

        const res = await fetch('/test/x');
        expect(res.body).toEqual('hi from /test/:a, your param was x');
      });
      it('should properly handle trailing slashes when using query parameters', async () => {
        app.get('/?:a', ({ res, params }) => res.send('hi from /?' + params.a));
        app.get('/tomato?b=:a', ({ res, params }) =>
          res.send('hi from /tomato?b=' + params.a),
        );

        let res = await fetch('/?gz');
        expect(res.body).toEqual('hi from /?gz');
        res = await fetch('/tomato/?b=gz');
        expect(res.body).toEqual('hi from /tomato?b=gz');
      });
      it('should not handle trailing slashes when the option is set to ignore', async () => {
        app.config.routingTrailingSlashes = Trailing.Mode.Ignore;

        app.get('/test/:a/', ({ res, params }) =>
          res.send('hi from /test/:a, your param was ' + params.a),
        );

        const res = await fetch('/test/x');
        expect(res.statusCode).toEqual(404);
      });
    });
    describe('Built-in Middleware', () => {
      describe('Trailing Slashes', () => {
        it(`should ensure there's a trailing slash in always mode`, async () => {
          app.use(App.trailingSlash(Trailing.Mode.Always));
          app.use(({ req, res }) => res.send(req.path));

          expect((await fetch('/')).body).toEqual('/');
          expect((await fetch('/test')).body).toEqual('/test/');
          expect((await fetch('/test/')).body).toEqual('/test/');
          expect((await fetch('/test/a')).body).toEqual('/test/a/');
        });
      });
      describe('CORS', () => {
        test.todo('Add .cors() Testing');
      });
    });
  });
});
