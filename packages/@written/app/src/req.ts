import { AppwriteReq, AppwriteRequestHeaders } from '@written/appwrite-types';
import {
  AppwriteContext,
  AppwriteResponseObject,
} from '@written/appwrite-types';
import { ParamsDictionary } from '@written/route-parameters';

export type NextFunc = () =>
  | AppwriteResponseObject
  | Promise<AppwriteResponseObject>;

export type ParamCtx<Params extends ParamsDictionary = ParamsDictionary> =
  AppwriteContext & {
    next: NextFunc;
    params: Params;
    locals: Record<string, any>;
    req: Request<Params>;
  };

export class Request<Params extends ParamsDictionary = ParamsDictionary>
  implements AppwriteReq
{
  public bodyRaw: string = '';
  public headers: AppwriteRequestHeaders = AppwriteRequestHeaders.parse({}); // generate a blank one
  public path: string = '';
  public url: string = '';
  public method: string = '';
  public host: string = '';
  public scheme: 'http' | 'https' = 'http';
  public query: Record<string, string> = {};
  public queryString: string = '';
  public port: number = -1;
  public body?: any;
  // @ts-ignore
  public params: Params;
  public constructor(
    req: AppwriteReq & {
      next: NextFunc;
    },
  ) {
    for (const [k, v] of Object.entries(req)) this[k as keyof this] = v;
    Object.entries(this.headers).forEach(
      ([k, v]) => (this.headers[k.toLowerCase()] = v),
    );
  }
  public getHeader(headerName: string) {
    return this.headers[headerName.toLowerCase()];
  }
  public header = this.getHeader;
  public get = this.header;
}
