export enum TrailingSlashes {
  /** Remove/Trim trailing slashes - Note that we will *always* have it if there's no route prior to the query */
  Never,
  /** Add trailing slashes */
  Always,
  /** Ignore the presence of trailing slashes */
  Ignore,
}
export class Trailing {
  public static Mode = TrailingSlashes;
  /** Returns a function for stripping paths */
  public static getTrailingSlashStrippingFunction(mode: TrailingSlashes) {
    let strip = (path: string) => path;
    const stripTrail = (path: string) => path.replace(/(?<!^)\/(?=[#?]|$)/, '');
    const appendTrail = (path: string) =>
      path.replace(/\/?(\?|\#|$)/, (match, p1) => {
        return p1 ? '/' + p1 : '/';
      });
    switch (mode) {
      case TrailingSlashes.Never:
        strip = stripTrail;
        break;

      case TrailingSlashes.Always:
        strip = appendTrail;
        break;
    }
    return strip;
  }
  public static process(route: string, mode: TrailingSlashes) {
    return this.getTrailingSlashStrippingFunction(mode)(route);
  }
  public static strip(
    route: string,
    mode: TrailingSlashes = TrailingSlashes.Never,
  ) {
    return this.process(route, mode);
  }
  public static enforce(
    route: string,
    mode: TrailingSlashes = TrailingSlashes.Always,
  ) {
    return this.process(route, mode);
  }
}
export default Trailing;
