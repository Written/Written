import Ctx from '@written/mkctx';
import App, { ServerMode } from '@written/app';
import throwables, {
  clientException,
  error,
  exception,
  redirect,
  serverException,
} from './lib';

describe('@written/throwables', () => {
  let app: App;
  beforeEach(() => {
    app = new App().use(throwables()); // we can call the middleware twice if we want options
  });

  test('should run with just plain middleware & nothing going on', () => {
    app.server(ServerMode.Appwrite)(new Ctx('https://localhost/test'));
  });

  describe('redirect', () => {
    test('should redirect properly', async () => {
      app.get('/test', () => {
        throw redirect('https://example.com/', 302); // redirect with http 302; if you omit the code, we use 307.
      });
      const response = await app.server(ServerMode.Appwrite)(
        new Ctx('https://localhost/test'),
      );
      expect(response.headers?.location ?? response.headers?.Location).toEqual(
        'https://example.com/',
      );
      expect(response.statusCode).toEqual(302);
    });

    test('should default to 307', async () => {
      app.get('/test', () => {
        throw redirect('https://example.com/');
      });
      const response = await app.server(ServerMode.Appwrite)(
        new Ctx('https://localhost/test'),
      );
      expect(response.headers?.location ?? response.headers?.Location).toEqual(
        'https://example.com/',
      );
      expect(response.statusCode).toEqual(307);
    });

    test('should not require me to throw it', async () => {
      app.get('/test', ({ res }) => {
        redirect('https://example.com/');
        return res.json({});
      });
      const response = await app.server(ServerMode.Appwrite)(
        new Ctx('https://localhost/test'),
      );
      expect(response.headers?.location ?? response.headers?.Location).toEqual(
        'https://example.com/',
      );
      expect(response.statusCode).toEqual(307);
    });

    test.todo('should ensure the callback gets called');
  });

  describe('error', () => {
    test('should error properly, defaulting to 500', async () => {
      app.get('/test', () => {
        throw error();
      });
      const response = await app.server(ServerMode.Appwrite)(
        new Ctx('https://localhost/test'),
      );
      expect(JSON.parse(response.body)).toEqual({
        error: 'No message specified',
        success: false,
      });
      expect(response.statusCode).toEqual(500);
    });

    test('should have exception and error behave identically', async () => {
      let fn = error;
      app.get('/test', () => {
        throw fn();
      });
      const response = await app.server(ServerMode.Appwrite)(
        new Ctx('https://localhost/test'),
      );
      fn = exception;
      const response2 = await app.server(ServerMode.Appwrite)(
        new Ctx('https://localhost/test'),
      );
      expect(response).toEqual(response2);
    });

    test('should overwrite error code when passed', async () => {
      app.get('/test', () => {
        throw error(400);
      });
      const response = await app.server(ServerMode.Appwrite)(
        new Ctx('https://localhost/test'),
      );
      expect(response.statusCode).toEqual(400);
    });

    test('should pass error message', async () => {
      app.get('/test', () => {
        throw error(400, 'Test!');
      });
      const response = await app.server(ServerMode.Appwrite)(
        new Ctx('https://localhost/test'),
      );
      expect(JSON.parse(response.body)).toEqual({
        error: 'Test!',
        success: false,
      });
      expect(response.statusCode).toEqual(400);
    });

    test('should default to 400 for clientError', async () => {
      app.get('/test', () => {
        throw clientException('test message');
      });
      const response = await app.server(ServerMode.Appwrite)(
        new Ctx('https://localhost/test'),
      );
      expect(response.statusCode).toEqual(400);
    });

    test('should pass message as first arg to clientError', async () => {
      app.get('/test', () => {
        throw clientException('test message');
      });
      const response = await app.server(ServerMode.Appwrite)(
        new Ctx('https://localhost/test'),
      );
      expect(JSON.parse(response.body)).toEqual({
        error: 'test message',
        success: false,
      });
    });

    test('should not require me to throw it', async () => {
      app.get('/test', ({ res }) => {
        error();
        return res.json({});
      });
      const response = await app.server(ServerMode.Appwrite)(
        new Ctx('https://localhost/test'),
      );
      expect(response.statusCode).toEqual(500);
    });

    test.todo('should ensure the callback gets called');
  });
});
