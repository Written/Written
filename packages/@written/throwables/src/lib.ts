import { MiddlewareFunction } from '@written/app';
import * as Classes from './classes';
import {
  AppwriteContext,
  AppwriteResponseObject,
  RedirectCodes,
} from '@written/appwrite-types';
import util from 'util';

export type Options = {
  /** Handles the Redirection Class - return null to provide the default response */
  redirectionHandler?: (
    redirect: Classes.Redirect,
    context: AppwriteContext,
  ) => AppwriteResponseObject | null;
  /** Handles the Exception Class - return null to provide the default response */
  exceptionHandler?: (
    exception: Classes.Exception,
    context: AppwriteContext,
  ) => AppwriteResponseObject | null;
  /** Catches exceptions that are not Throwables/Exceptions */
  uncaughtHandler?: (
    thrown: any,
    context: AppwriteContext,
  ) => AppwriteResponseObject;
  /**
   * Handle all uncaughts as 5xxs; uncaughtHandler takes precedence over this
   * @default true
   */
  handleUncaughtsAs5xx?: boolean;
  /**
   * Whether to reveal the error info in {@link handleUncaughtsAs5xx}
   * @default false
   */
  revealUncaughtErrorDetails?: boolean;
};

const rawThrowables =
  (options: Options = {}): MiddlewareFunction =>
  async ctx => {
    const { res, next } = ctx;
    options.handleUncaughtsAs5xx = options.handleUncaughtsAs5xx ?? true;
    try {
      return await next();
    } catch (error) {
      const handleAsException = (
        isServer = error instanceof Classes.ServerException &&
          error.code.toString().startsWith('5'),
      ) => {
        if (!error || !(error instanceof Classes.Exception)) {
          const fmt = util.format(error);
          error = new Classes.ServerException(
            `Encountered Internal Error${
              options.revealUncaughtErrorDetails
                ? `:
${fmt}`
                : ''
            }`,
          );
          ctx.error(
            'Encountered not instanceof Classes.Exception in handleAsException():',
          );
          ctx.error(fmt);
        }
        return (
          options?.exceptionHandler?.(error as Classes.Exception, ctx) ??
          ((error: Classes.Exception) => {
            if (isServer)
              ctx?.error?.(
                `Encountered ServerError: ${error.stack ?? error.message}`,
              );
            return res.json(
              {
                success: false,
                error: error.message,
              },
              error?.code ?? 500,
              {
                'content-type': 'application/json',
              },
            );
          })(error as Classes.Exception)
        );
      };
      if (error instanceof Classes.Exception) return handleAsException();
      else if (error instanceof Classes.Redirect)
        return (
          options?.redirectionHandler?.(error, ctx) ??
          res.redirect(error.target, error.code)
        );
      else if (options.uncaughtHandler)
        try {
          const rt = options.uncaughtHandler(error, ctx);
          if (rt) return rt;
        } catch (error) {
          ctx.error('Uncaught Handler Failed with');
          ctx.error(util.format(error));
        }
      if (options.handleUncaughtsAs5xx) return handleAsException(true);
      throw error;
    }
  };

/** Throw an exception */
export const exception = (code = 500, message?: string) => {
  throw new Classes.Exception(message, code);
};
/** Alias to {@link exception} */
export const error = exception;
/** Throw a client exception */
export const clientException = (message?: string, code = 400) => {
  throw new Classes.ClientException(message, code);
};
/** Throw a server exception */
export const serverException = (message?: string, code = 500) => {
  throw new Classes.ServerException(message, code);
};
/** Redirects to the target URL */
export const redirect = (
  target: string,
  code: (typeof RedirectCodes)[keyof typeof RedirectCodes] = RedirectCodes.TemporaryRedirect,
) => {
  throw new Classes.Redirect(target, code);
};

export const throwables = rawThrowables as typeof rawThrowables & {
  // add the funny functions as direct members of throwables cuz :shrug: why not
  error: typeof error;
  exception: typeof exception;
  clientException: typeof clientException;
  serverException: typeof serverException;
  redirect: typeof redirect;
};
throwables.error = error;
throwables.exception = exception;
throwables.clientException = clientException;
throwables.serverException = serverException;
throwables.error = error;

export default throwables;
export { Classes };
