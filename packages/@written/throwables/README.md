<div align="center">

![Written Logo](https://codeberg.org/Written/Written/raw/branch/senpai/ico/written.png)

# @written/throwables

Make Appwrite Error Handling simpler. Inspired heavily by [Svelte Errors](https://learn.svelte.dev/tutorial/error-basics)

[![Codeberg](https://img.shields.io/badge/Codeberg-Written-724773.svg?style=flat-square)](https://codeberg.org/Written/Written/src/branch/senpai/packages/@written/throwables)
[![License](https://img.shields.io/badge/License-MIT-724773.svg?style=flat-square)](https://codeberg.org/Written/Written/src/branch/senpai/LICENSE)

</div>

## Installation

In your function, run:

```sh
pnpm i @written/throwables
```

## Usage

```ts
import throwables, {
  clientException,
  serverException,
  redirect,
} from '@written/throwables';

const app = new App().use(throwables()); // use the throwables middleware

app.get('/err', ({ req, res }) => {
  throw clientException(400, 'this is an error endpoint');
});

app.get('/fuckup', ({ req, res }) => {
  throw serverException(500, 'whoops, we fucked up!'); // this will log the stack while we're at it in the default handler
});

app.get('/redirect', ({ req, res }) => {
  throw redirect('https://example.com/', 302); // redirect with http 302; if you omit the code, we use 307.
});

export default app.server();
```

For both `clientException` and `serverException`, the resulting response's body has the shape `{success:false,error:<reason>}`, where `reason` defaults to `No message specified`
