# @written/throwables

## 2.0.4

### Patch Changes

- a748109: Update Deps
- Updated dependencies [a748109]
  - @written/appwrite-types@0.2.17
  - @written/app@0.5.4

## 2.0.3

### Patch Changes

- 51d9506: Upgrade Dependencies
- Updated dependencies [51d9506]
  - @written/appwrite-types@0.2.16
  - @written/app@0.5.3

## 2.0.2

### Patch Changes

- d1ea241: Update Deps
- Updated dependencies [d1ea241]
  - @written/appwrite-types@0.2.15
  - @written/app@0.5.2

## 2.0.1

### Patch Changes

- Updated dependencies [1df6d84]
  - @written/appwrite-types@0.2.14
  - @written/app@0.5.1

## 2.0.0

### Patch Changes

- 4fa1758: Upgrade dependencise
- a0c9930: Upgrade Dependencies
- Updated dependencies [034c27f]
- Updated dependencies [4fa1758]
- Updated dependencies [fbd6360]
- Updated dependencies [39ce77a]
- Updated dependencies [a0c9930]
  - @written/appwrite-types@0.2.13
  - @written/app@0.5.0

## 1.0.9

### Patch Changes

- Updated dependencies [816cec3]
  - @written/appwrite-types@0.2.12
  - @written/app@0.4.9

## 1.0.8

### Patch Changes

- 3872f26: Add LICENSE files
- bee1c23: Move repos to Written/Written
- de25848: Consistent, Clean README header format
- Updated dependencies [8acd2e0]
- Updated dependencies [bee1c23]
- Updated dependencies [de25848]
  - @written/appwrite-types@0.2.11
  - @written/app@0.4.8

## 1.0.7

### Patch Changes

- 40f46fa: Upgrade Dependencies
- Updated dependencies [40f46fa]
  - @written/appwrite-types@0.2.10
  - @written/app@0.4.7

## 1.0.6

### Patch Changes

- 4a5dd4d: bump dependencies
- Updated dependencies [4a5dd4d]
  - @written/appwrite-types@0.2.9
  - @written/app@0.4.6

## 1.0.5

### Patch Changes

- rebuild everything due to a filesystem error during the last build
- Updated dependencies
  - @written/app@0.4.5
  - @written/appwrite-types@0.2.8

## 1.0.4

### Patch Changes

- cc4fcee: Use the ~ version range
- Updated dependencies [cc4fcee]
  - @written/app@0.4.4

## 1.0.3

### Patch Changes

- bump everything
- Updated dependencies
  - @written/app@0.4.3
  - @written/appwrite-types@0.2.7

## 1.0.2

### Patch Changes

- 18c6dd1: fix: use es6 in lib, i hate you typescript
- Updated dependencies [18c6dd1]
  - @written/appwrite-types@0.2.6
  - @written/app@0.4.2

## 1.0.1

### Patch Changes

- 48bc017: Upgrade Dependencies
  - @written/app@0.4.1

## 1.0.0

### Patch Changes

- cc261b7: Properly handle uncaught 5xx's
- 1208d0d: Strict Mode, for everything
- Updated dependencies [48692e6]
- Updated dependencies [78b268e]
- Updated dependencies [3ae4ab7]
- Updated dependencies [4301ecc]
- Updated dependencies [48692e6]
- Updated dependencies [1208d0d]
  - @written/app@0.4.0
  - @written/appwrite-types@0.2.5

## 0.1.5

### Patch Changes

- 2386f87: Force Content-Type

## 0.1.4

### Patch Changes

- b29ff7f: Add option to handle uncaughts
- d069256: properly send errors via res.json instead of res.send
- Updated dependencies [bca8b56]
  - @written/appwrite-types@0.2.4
  - @written/app@0.3.4

## 0.1.3

### Patch Changes

- Updated dependencies [825ec04]
  - @written/app@0.3.3

## 0.1.2

### Patch Changes

- 4b501e8: update the readme to specify the error shape
  - @written/app@0.3.2
